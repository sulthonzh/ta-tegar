 (function($) {

     $("#demo-form").on("submit", function(e) {
         e.preventDefault();

         obj = $(this).serializeObject();

         $.ajax({
             url: "/api/v1/scholarship/" + obj['id'] + "/calculate-demo",
             type: 'GET',
             contentType: 'application/json',
             dataType: "json",
             success: function(data) {
                 // empty div#wizard_verticle 
                 $('#wizard_verticle').empty();

                 var htmlStep = '<ul class="wizard_steps">';
                 var htmlStepContent = '';

                 var addStep = function(key, value, disabled) {
                     status = ""
                     if (disabled === true) { status = ` class="disabled"` }
                     htmlStep += `<li><a href="#step-` + key + `" ` + status + `><span class="step_no">` + key + `</span><span class="step_descr">Step ` + key + `<br /><small>Step ` + key + ` ` + value.toUpperCase() + `</small></span></a></li>`;
                 };
                 var addStepContent = function(key, value, disabled) {
                     htmlStepContent += `<div id="step-` + key + `"><p></p></div>`;
                 }

                 var addStepContentLoop = function(key, value, disabled) {
                     table = '';

                     $.each(value, function(k, v) {
                         table += `<pre><code data-language="coffeescript">` + v.replace('\n', '&#13;&#10;') + `</code></pre>`;
                     });

                     console.log(table);
                     htmlStepContent += `<div id="step-` + key + `">` + table + `</div>`;
                 };

                 addStep(1, "Start", false);
                 addStepContent(1, "Start", false)

                 $.each(data["AHP"]["parameters"], function(key, value) {

                     content = data["AHP"]["algorithmResult"][key];

                     addStep(key + 2, value.name, true);
                     addStepContentLoop(key + 2, content, true);
                 });

                 addStep(data["AHP"]["parameters"].length + 2, "Hasil", true);
                 addStepContent(data["AHP"]["parameters"].length + 2, "Hasil", true);

                 htmlStep += `</ul>`;

                 $('#wizard_verticle').append(htmlStep);
                 $('#wizard_verticle').append(htmlStepContent);

                 console.log(data);

             },
             complete: function() {
                 // wait for 250 ms, then try and retrieve contents
                 init_SmartWizard()
                 id = $('[name=id]').val();
             },


         });

     });
     $('.buttonFinish').on('click', function(e) {
         //  $('.buttonFinish').attr("href", 'http://localhost:9090/scholarship/' + id + '?#result');
     });


 })(jQuery);