$(function() {
    $(".table").on("click", "tr[role=\"button\"]", function(e) {
        window.location = $(this).data("href");
    });

    $(".table").on("click", "button[role=\"delete\"]", function(e) {
        href = $(this).closest("tr").data("href")
        ok = confirm("Apakah anda yakin ingin menghapus data '" + $(this).closest("tr").children().eq(1).text() + "'?");
        if (ok) {
            $.ajax({
                url: "/api/v1/" + href,
                type: 'DELETE',
                success: function(data) {},
                complete: function() {
                    window.location = "/student"
                },
            });
        }
        return false;
    });
});