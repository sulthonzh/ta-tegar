function chart(mapeIndex) {

    if (typeof($.plot) === 'undefined') { return; }
    mapeIndex = parseInt(mapeIndex);
    console.log('init_flot_chart');
    var chart_plot_01_settings = {
        series: {
            lines: {
                show: false,
                fill: true
            },
            splines: {
                show: true,
                tension: 0.4,
                lineWidth: 1,
                fill: 0.4
            },
            points: {
                radius: 0,
                show: true
            },
            shadowSize: 2
        },
        grid: {
            verticalLines: true,
            hoverable: true,
            clickable: true,
            tickColor: "#d5d5d5",
            borderWidth: 1,
            color: '#fff'
        },
        colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
        xaxis: {
            tickColor: "rgba(51, 51, 51, 0.06)",
            mode: "time",
            tickSize: [6, "month"],
            //tickLength: 10,
            axisLabel: "Date",
            axisLabelUseCanvas: true,
            axisLabelFontSizePixels: 12,
            axisLabelFontFamily: 'Verdana, Arial',
            axisLabelPadding: 10
        },
        yaxis: {
            ticks: 8,
            tickColor: "rgba(51, 51, 51, 0.06)",
        },
        tooltip: {
            show: true,
            shifts: {
                x: -30,
                y: -50
            },
            defaultTheme: false
        },
    }



    $.ajax({
        url: "/api/v1/forecast/",
        type: 'GET',
        contentType: 'application/json',
        dataType: "json",
        success: function(data) {
            var arr1 = [];
            var arr2 = [];
            console.log(data);
            for (i = 0; i < data.time.length; i++) {
                var time = [];
                if (data.time[i][1] === 12) {
                    time.push(data.time[i][0] + 1);
                    time.push(1);
                } else {
                    time.push(data.time[i][0]);
                    time.push(data.time[i][1] + 1)
                }

                arr1.push([gd(time[0], time[1], 1), data.data[i]]);
            }


            var mapeTemp;
            var mapeIndexTemp;

            for (j = 0; j < data.forecasting.length; j++) {
                var arr2Temp = [];
                for (k = 0; k < data.forecasting[j].ft.length; k++) {
                    var time = [];
                    if (k === data.forecasting[j].ft.length - 1) {
                        time.push(data.time.slice(-1)[0][0] + 1);
                        time.push(2);
                    } else {
                        var tahun = data.time[k][0];
                        var bulan = data.time[k][1];

                        if (bulan + 1 === 13) {
                            time.push(tahun + 1);
                            time.push(1);
                        } else {
                            time.push(tahun);
                            time.push(bulan + 1)
                        }
                    }

                    arr2Temp.push([gd(time[0], time[1], 1), data.forecasting[j].ft[k]]);
                }
                arr2.push(arr2Temp);

                if (j === 0) {
                    mapeTemp = data.forecasting[j].mape;
                } else {
                    if (data.forecasting[j].mape < mapeTemp) {
                        mapeTemp = data.forecasting[j].mape;
                        mapeIndexTemp = j
                    }
                }
            }
            // Sesuai user
            if (mapeIndex === -1) {
                mapeIndex = mapeIndexTemp;
            }



            var chart_plot_02_settings = {
                grid: {
                    show: true,
                    aboveData: true,
                    color: "#3f3f3f",
                    labelMargin: 10,
                    axisMargin: 0,
                    borderWidth: 0,
                    borderColor: null,
                    minBorderMargin: 5,
                    clickable: true,
                    hoverable: true,
                    autoHighlight: true,
                    mouseActiveRadius: 100
                },
                series: {
                    lines: {
                        show: true,
                        fill: false,
                        lineWidth: 2,
                        steps: false
                    },
                    points: {
                        show: true,
                        radius: 4.5,
                        symbol: "circle",
                        lineWidth: 3.0
                    }
                },
                legend: {
                    position: "ne",
                    margin: [0, -25],
                    noColumns: 0,
                    labelBoxBorderColor: null,
                    labelFormatter: function(label, series) {
                        return label + '&nbsp;&nbsp;';
                    },
                    width: 40,
                    height: 1
                },
                colors: ['#633517', '#a6001a', '#e06000', '#ee9600', '#ffab00', '#004d33', '#00477e'],
                shadowSize: 0,
                tooltip: true,
                tooltipOpts: {
                    content: "%x: %y.0 orang",
                    xDateFormat: "%m-%Y",
                    shifts: {
                        x: -30,
                        y: -50
                    },
                    defaultTheme: false
                },
                yaxis: {
                    ticks: 10,
                    // tickColor: "rgba(51, 51, 51, 0.06)",
                },

                xaxis: {
                    tickColor: "rgba(51, 51, 51, 0.06)",
                    mode: "time",
                    tickSize: [6, "month"],
                    //tickLength: 10,
                    axisLabel: "Date",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 10
                },
            };
            console.log(mapeIndex + "-" + mapeIndexTemp);
            if ($("#chart_plot_02").length) {
                console.log('Plot1');
                $.plot($("#chart_plot_02"), [arr1, arr2[mapeIndex]], chart_plot_02_settings);
            }
        },
    });


    // var arr_data1 = [
    //     [gd(2012, 1, 0), 17],
    //     [gd(2012, 1), 74],
    //     [gd(2012, 1), 6],
    //     [gd(2012, 1), 39],
    //     [gd(2012, 1), 20],
    //     [gd(2012, 1), 85],
    //     [gd(2012, 1), 7]
    // ];

    // var arr_data2 = [
    //     [gd(2012, 1, 1), 82],
    //     [gd(2012, 1, 2), 23],
    //     [gd(2012, 1, 3), 66],
    //     [gd(2012, 1, 4), 9],
    //     [gd(2012, 1, 5), 119],
    //     [gd(2012, 1, 6), 6],
    //     [gd(2012, 1, 7), 9]
    // ];

    // var arr_data3 = [
    //     [0, 1],
    //     [1, 9],
    //     [2, 6],
    //     [3, 10],
    //     [4, 5],
    //     [5, 17],
    //     [6, 6],
    //     [7, 10],
    //     [8, 7],
    //     [9, 11],
    //     [10, 35],
    //     [11, 9],
    //     [12, 12],
    //     [13, 5],
    //     [14, 3],
    //     [15, 4],
    //     [16, 9]
    // ];

    // var chart_plot_02_data = [];

    // var chart_plot_03_data = [
    //     [0, 1],
    //     [1, 9],
    //     [2, 6],
    //     [3, 10],
    //     [4, 5],
    //     [5, 17],
    //     [6, 6],
    //     [7, 10],
    //     [8, 7],
    //     [9, 11],
    //     [10, 35],
    //     [11, 9],
    //     [12, 12],
    //     [13, 5],
    //     [14, 3],
    //     [15, 4],
    //     [16, 9]
    // ];


    // for (var i = 0; i < 30; i++) {
    //     chart_plot_02_data.push([new Date(Date.today().add(i).days()).getTime(), randNum() + i + i + 10]);
    // }




    // var chart_plot_02_settings = {
    //     grid: {
    //         show: true,
    //         aboveData: true,
    //         color: "#3f3f3f",
    //         labelMargin: 10,
    //         axisMargin: 0,
    //         borderWidth: 0,
    //         borderColor: null,
    //         minBorderMargin: 5,
    //         clickable: true,
    //         hoverable: true,
    //         autoHighlight: true,
    //         mouseActiveRadius: 100
    //     },
    //     series: {
    //         lines: {
    //             show: true,
    //             fill: true,
    //             lineWidth: 2,
    //             steps: false
    //         },
    //         points: {
    //             show: true,
    //             radius: 4.5,
    //             symbol: "circle",
    //             lineWidth: 3.0
    //         }
    //     },
    //     legend: {
    //         position: "ne",
    //         margin: [0, -25],
    //         noColumns: 0,
    //         labelBoxBorderColor: null,
    //         labelFormatter: function(label, series) {
    //             return label + '&nbsp;&nbsp;';
    //         },
    //         width: 40,
    //         height: 1
    //     },
    //     colors: ['#96CA59', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'],
    //     shadowSize: 0,
    //     tooltip: true,
    //     tooltipOpts: {
    //         content: "%s: %y.0",
    //         xDateFormat: "%d/%m",
    //         shifts: {
    //             x: -30,
    //             y: -50
    //         },
    //         defaultTheme: false
    //     },
    //     yaxis: {
    //         min: 0
    //     },
    //     xaxis: {
    //         mode: "time",
    //         minTickSize: [1, "day"],
    //         timeformat: "%d/%m/%y",
    //         min: chart_plot_02_data[0][0],
    //         max: chart_plot_02_data[20][0]
    //     }
    // };

    // var chart_plot_03_settings = {
    //     series: {
    //         curvedLines: {
    //             apply: true,
    //             active: true,
    //             monotonicFit: true
    //         }
    //     },
    //     colors: ["#26B99A"],
    //     grid: {
    //         borderWidth: {
    //             top: 0,
    //             right: 0,
    //             bottom: 1,
    //             left: 1
    //         },
    //         borderColor: {
    //             bottom: "#7F8790",
    //             left: "#7F8790"
    //         }
    //     }
    // };


    // if ($("#chart_plot_01").length) {
    //     console.log('Plot1');

    //     $.plot($("#chart_plot_01"), [arr_data1, arr_data2], chart_plot_01_settings);
    // }


    // if ($("#chart_plot_02").length) {
    //     console.log('Plot2');

    //     $.plot($("#chart_plot_02"), [{
    //         label: "Email Sent",
    //         data: chart_plot_02_data,
    //         lines: {
    //             fillColor: "rgba(150, 202, 89, 0.12)"
    //         },
    //         points: {
    //             fillColor: "#fff"
    //         }
    //     }], chart_plot_02_settings);

    // }

    // if ($("#chart_plot_03").length) {
    //     console.log('Plot3');


    //     $.plot($("#chart_plot_03"), [{
    //         label: "Registrations",
    //         data: chart_plot_03_data,
    //         lines: {
    //             fillColor: "rgba(150, 202, 89, 0.12)"
    //         },
    //         points: {
    //             fillColor: "#fff"
    //         }
    //     }], chart_plot_03_settings);

    // };

}


function pasien_statistik(kelas) {
    $.ajax({
        url: "/api/v1/pasien/?limit=-1",
        type: 'GET',
        contentType: 'application/json',
        dataType: "json",
        success: function(data) {
            var pk = {
                "vip": [],
                "1a": [],
                "1b": [],
                "2a": [],
                "2b": [],
                "3a": [],
                "3b": [],
            };


            data.forEach(function(item, index) {

                // newUnixTimeStamp = moment(item.created_at, 'YYYY-MM-DD HH:MM:ss').unix();

                var value = [new Date.parse(item.created_at), item.jumlah];
                pk[item.kelas].push(value);

            });

            var chart_plot_02_settings = {
                grid: {
                    show: true,
                    aboveData: true,
                    color: "#3f3f3f",
                    labelMargin: 10,
                    axisMargin: 0,
                    borderWidth: 0,
                    borderColor: null,
                    minBorderMargin: 5,
                    clickable: true,
                    hoverable: true,
                    autoHighlight: true,
                    mouseActiveRadius: 100
                },
                series: {
                    lines: {
                        show: false,
                        fill: true
                    },
                    splines: {
                        show: true,
                        tension: 0.4,
                        lineWidth: 1,
                        fill: 0.4
                    },
                    points: {
                        radius: 0,
                        show: true
                    },
                    shadowSize: 2
                },
                legend: {
                    position: "ne",
                    margin: [0, -25],
                    noColumns: 0,
                    labelBoxBorderColor: null,
                    labelFormatter: function(label, series) {
                        return label + '&nbsp;&nbsp;';
                    },
                    width: 40,
                    height: 1
                },
                colors: ['#633517', '#a6001a', '#e06000', '#ee9600', '#ffab00', '#004d33', '#00477e'],
                shadowSize: 0,
                tooltip: true,
                tooltipOpts: {
                    content: "%x: %y.0 orang",
                    xDateFormat: "%d-%m-%Y",
                    shifts: {
                        x: -30,
                        y: -50
                    },
                    defaultTheme: false
                },
                yaxis: {
                    ticks: 10,
                    // tickColor: "rgba(51, 51, 51, 0.06)",
                },

                xaxis: {
                    tickColor: "rgba(51, 51, 51, 0.06)",
                    mode: "time",
                    tickSize: [6, "month"],
                    //tickLength: 10,
                    axisLabel: "Date",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 10
                },
            };

            statData = [];
            if (kelas === "all") {
                statData = [pk["vip"], pk["1a"], pk["1b"], pk["2a"], pk["2b"], pk["3a"], pk["3b"]];
            } else {
                statData = [pk[kelas]];
            }

            if ($("#chart_plot_01").length) {
                console.log('Plot1');
                $.plot($("#chart_plot_01"), statData, chart_plot_02_settings);
            }
            console.log(pk);
        },
    });
}

function dash() {
    filter = "year";
    dataFilter = {
        "year": "tahun",
        "week": "minggu",
    };


    $.ajax({
        url: "/api/v1/dash/",
        type: 'GET',
        contentType: 'application/json',
        dataType: "json",
        success: function(data) {
            $('#dash-total > div').text(data.total);
            for (var kelas in data.last_year) {
                $('#dash-' + kelas + ' > div').text(data.last_year[kelas][0]);

                s = "asc";
                c = "green";
                if (data.last_year[kelas][2] < 0) {
                    c = "red";
                    s = "desc";
                }
                $('#dash-' + kelas + ' .count_bottom > i > i').attr("class", "fa fa-sort-" + s);
                $('#dash-' + kelas + ' .count_bottom > i > i')[0].nextSibling.nodeValue = Math.abs(data.last_year[kelas][2]) + '%';

                $('#dash-' + kelas + ' .count_bottom > i').attr("class", c);
                $('#dash-' + kelas + ' .count_bottom > i')[0].nextSibling.nodeValue = ' dari ' + dataFilter[filter] + ' sebelumnya';
            }
        }
    });
}
$(document).ready(function() {
    chart(-1);
    pasien_statistik("all");
    dash()
});


$(function() {
    $('#hitung-btn').on('click', function(e) {
        chart($('#alpha-input').val());
    });

    $('#show-pasien-btn').on('click', function(e) {
        pasien_statistik($('#kelas-input').val());
    });
});