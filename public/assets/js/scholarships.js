$(function() {

    $(".table").on("click", "tr[role=\"button\"]", function(e) {
        window.location = $(this).data("href");
    });

    $(".table tr > td").on("click", "button[role=\"delete\"]", function(e) {
        e.preventDefault();
        hapus = confirm("Apakah anda yakin ingin menghapus data '" + $(this).closest("tr").children().eq(1).text() + "'?");

        if (hapus) {
            $.ajax({
                url: "/api/v1/scholarship/" + $(this).closest("tr").attr("id"),
                type: 'DELETE',
                success: function(data) {

                },
                complete: function() {
                    location.reload();
                },
            });
        }
    });





});