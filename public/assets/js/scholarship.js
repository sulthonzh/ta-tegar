(function($) {
    $(".table").on("click", "tr[role=\"button\"]", function(e) {
        window.location = $(this).data("href");
    });

    $("[role=\"menu\"] li:contains(\"Hitung\")").on("click", function(e) {
        scholarship_id = $("[name=id]").val();
        $.ajax({
            url: "/api/v1/scholarship/" + scholarship_id + "/calculate",
            type: 'GET',
            contentType: 'application/json',
            dataType: "json",
            success: function(data) {
                console.log(data);
            },
            complete: function() {
                location.reload();
            },
        });
    });

    $("[role=\"menu\"] li:contains(\"Clear\")").on("click", function(e) {
        scholarship_id = $("[name=id]").val();
        $.ajax({
            url: "/api/v1/scholarship/" + scholarship_id + "/clear",
            type: 'GET',
            contentType: 'application/json',
            dataType: "json",
            success: function(data) {
                console.log(data);
            },
            complete: function() {
                location.reload();
            },
        });
    });

    $("#pi-form").on("submit", function(e) {
        e.preventDefault();

        scholarship_id = $("[name=id]").val();
        action_id = $("[name=id_action]").val();

        obj = $(this).serializeObject();



        if (action_id == "create") {
            obj["participants"] = [];
            console.log(obj);

            $.ajax({
                url: "/api/v1/scholarship",
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify(obj),
                success: function(data) {
                    console.log(data);
                },
                complete: function() {
                    location.reload();
                },
            });
        } else {

            $.ajax({
                url: "/api/v1/scholarship",
                type: 'PATCH',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify(obj),
                success: function(data) {
                    console.log(data);
                },
                complete: function() {
                    location.reload();
                },
            });
        }
    });

    $("#fs-form").on("submit", function(e) {
        e.preventDefault();
        $.ajax({
            url: "/api/v1/scholarship/participant?" + $(this).serialize(),
            type: 'GET',
            success: function(data) {
                console.log(data);
            },
            complete: function() {
                location.reload();
            },
        });
    });


})(jQuery);