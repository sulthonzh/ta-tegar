function init_daterangepicker_single_call() {
    d = $('#st-birthday');
    d.datepicker({ 
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd',
        yearRange: "1930:-5"
     });

    if (d.text() != "") {
        console.log(d.text())
        d.text(d.text().replace(" 07:00:00 +0700 WIB"))
    }
}


$(function() {
    init_daterangepicker_single_call()

    $("#pi-form").on("submit", function(e) {
        e.preventDefault()


        var temp = $(this).serializeObject();
        temp['birthday'] += 'T00:00:00.000Z'
        temp['gender'] = parseInt(temp['gender']);
        temp['class'] = parseInt(temp['class']);
        temp['parentInfo']['father']['profession'] = parseInt(temp['parentInfo']['father']['profession']);
        console.log(temp)

        url = window.location.href.toString().split("/");
        console.log(url)
        console.log(temp['id'])
        console.log( url[url.length - 1] )

        if (temp['id'] != "" && url[url.length - 1] != "create") {
            $.ajax({
                url: "/api/v1/student/info",
                type: 'PATCH',
                contentType: 'application/json',
                data: JSON.stringify(temp),
                success: function(data) {},
                complete: function() {},
            });
        } else {

            $.ajax({
                url: "/api/v1/student",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(temp),
                success: function(data) {},
                complete: function() {
                },
            });


        }
    });
    $("#si-form").on("submit", function(e) {
        e.preventDefault()
        var temp = $(this).serializeObject();
        var input = {
            'gaji': parseInt(temp['parentsSalary']),
            'absensi': parseInt(temp['absence']),
            'nilai': parseInt(temp['score']),
            'perilaku': parseInt(temp['behavior']),
        }
        url = window.location.href.toString().split("/");
        // console.log(url.substring(url.lastIndexOf('/') + 1, url.length))

        if (temp['id'] != "" && url[url.length - 1] != "create") {
            $.ajax({
                url: "/api/v1/student/" + temp['id'],
                type: 'PATCH',
                contentType: 'application/json',
                data: JSON.stringify(input),
                success: function(data) {},
                complete: function() {},
            });

        }
    });
});