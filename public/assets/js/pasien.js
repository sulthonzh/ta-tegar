$(function() {
    $('#pi-tanggal').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: "YYYY-MM-DD",
        }
    });

    $('#pi-form').on('submit', function(e) {
        e.preventDefault();

        $.ajax({
            url: "/api/v1/pasien/",
            type: 'POST',
            contentType: 'application/x-www-form-urlencoded',
            dataType: "json",
            data: $(this).serialize(),
            success: function(data) {},
            complete: function() {
                location.reload();
            },
        });
    });

    $('.pi-edit').on('click', function(e) {
        $.ajax({
            url: "/api/v1/pasien/" + $(this).val(),
            type: 'GET',
            dataType: "json",
            success: function(data) {
                $('#pi-tanggal').val(data.created_at);
                $('#pi-kelas').val(data.kelas);
                $('#pi-jumlah').val(data.jumlah);
                $('#pi-form button[type=submit]').text("Update")
            }
        });
    });

    $('.pi-delete').on('click', function(e) {
        $.ajax({
            url: "/api/v1/pasien/" + $(this).val(),
            type: 'DELETE',
            dataType: "json",
            success: function(data) {},
            complete: function() {
                location.reload();
            },
        });

    });

});