$(function() {
$("#pi-form").on("submit", function(e) {
    e.preventDefault();

    scholarship_id = $("[name=id]").val();
    action_id = $("[name=id_action]").val();

    obj = $(this).serializeObject();



    if (action_id == "create") {
        obj["participants"] = [];
        console.log(obj);

        $.ajax({
            url: "/api/v1/scholarship/participants",
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(obj),
            success: function(data) {
                console.log(data);
            },
            complete: function() {
                location.reload();
            },
        });
    } else {

        $.ajax({
            url: "/api/v1/scholarship",
            type: 'PATCH',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(obj),
            success: function(data) {
                console.log(data);
            },
            complete: function() {
                location.reload();
            },
        });
    }
});

});