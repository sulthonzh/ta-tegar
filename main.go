package main

import (
	"log"
	"os"
	"os/exec"
	"runtime"
	"time"

	"github.com/facebookgo/grace/gracehttp"
	"github.com/labstack/echo"
	mw "github.com/labstack/echo/middleware"

	"gitlab.com/sulthonzh/tegar/config"
	"gitlab.com/sulthonzh/tegar/middleware"
	"gitlab.com/sulthonzh/tegar/routes"
	validator "gopkg.in/go-playground/validator.v9"
)

// Config is a function for load config
func main() {
	/*============
	APP INITIALIZE
	============*/
	config.Init()

	/*===============
	PRE-BINDING ROUTE
	===============*/
	e := echo.New()

	/*===========
	DEBUG SETTING
	===========*/
	e.Debug = true
	e.Pre(mw.RemoveTrailingSlash())

	/*============
	SET MIDDLEWARE
	============*/
	/*===========
	SET TEMPLATES
	===========*/
	t := &middleware.Template{}
	t.Init()

	e.Renderer = t
	e.Validator = &middleware.CustomValidator{Validator: validator.New()}

	e.Use(mw.Logger())                     // Logger middleware
	e.Use(mw.Recover())                    // Recover middleware
	e.Use(mw.CORSWithConfig(mw.CORSConfig{ // CORS
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE, echo.OPTIONS},
		AllowHeaders: []string{"Accept", "Content-Type", "Content-Length", "Accept-Encoding", "X-CSRF-Token", "Authorization"},
	}))

	// e.Use(mw.CSRFWithConfig(mw.CSRFConfig{
	// 	TokenLookup: "header:X-XSRF-TOKEN",
	// }))

	e.Use(mw.Secure())                     //Secure middleware
	e.Use(mw.GzipWithConfig(mw.GzipConfig{ //Gzip middleware
		Level: 5,
	}))

	/*================
	END SET MIDDLEWARE
	================*/

	/*===============
	ROUTES INITIALIZE
	===============*/
	routes.Init(e)

	/*=================
	SPECIALS CONDITIONS
	=================*/
	var cmd *exec.Cmd // Automatically open the web browser
	switch runtime.GOOS {
	case "windows":
		cmd = exec.Command("cmd", "/c", "start", "http://"+config.ServerAddress)
	case "darwin":
		cmd = exec.Command("open", "http://"+config.ServerAddress)
	}

	if cmd != nil {
		go func() {
			log.Printf("[T][TugasAkhir] Open the default browser after two seconds...")
			time.Sleep(time.Second * 2)
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			cmd.Run()
		}()
	}

	/*================
	SET SERVER ADDRESS
	================*/
	log.Printf("[T][TugasAkhir] Server running on %v", config.ServerAddress)
	/*===========
	START SERVICE
	===========*/
	e.Server.Addr = config.ServerAddress
	e.Logger.Fatal(gracehttp.Serve(e.Server))
	// e.Logger.Fatal(e.Start(config.ServerAddress))
	// e.Logger.Fatal(e.Server.ListenAndServe())

}
