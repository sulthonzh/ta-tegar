package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"gopkg.in/mgo.v2/bson"
	resty "gopkg.in/resty.v0"

	"gitlab.com/sulthonzh/tegar/models"

	"github.com/labstack/echo"
)

// PageStudents is a function for ...
func (h *Handler) PageStudents(c echo.Context) (err error) {

	students := []*models.Student{}
	resp, err := resty.R().
		SetMultiValueQueryParams(c.QueryParams()).
		SetHeader("Accept", "application/json").
		Get("http://127.0.0.1:9090/api/v1/student?" + c.QueryString())

	if resp.StatusCode() == 200 {
		err = json.Unmarshal(resp.Body(), &students)

		if err != nil {
			return
		}
	}

	data := map[string]interface{}{
		"page": "students.html",
		"app":  h.GlobalData["app"],
		"head": map[string]interface{}{
			"title": "Siswa | Penentuan Penerimaan Beasiswa",
		},
		"leftside": h.GlobalData["leftside"],
		"topnav":   h.GlobalData["topnav"],
		"contents": map[string]interface{}{
			"class":    c.QueryParam("class"),
			"students": students,
		},
		"user": h.GlobalData["user"],
		"assets": map[string]interface{}{
			"js": []string{
				"/assets/js/students.js",
			},
		},
	}

	return c.Render(http.StatusOK, "main.html", data)
}

// PageStudent is a function for ...
func (h *Handler) PageStudent(c echo.Context) (err error) {
	studentID := c.Param("student_id")
	op := ""
	student := models.Student{}

	if studentID == "create" {
		op = "create"
		student.Id = bson.NewObjectId()
		student.CreatedAt = student.Id.Time()
	} else {
		resp, err := resty.R().
			SetMultiValueQueryParams(c.QueryParams()).
			SetHeader("Accept", "application/json").
			Get("http://127.0.0.1:9090/api/v1/student/" + studentID)

		if resp.StatusCode() == 200 {
			err = json.Unmarshal(resp.Body(), &student)

			if err != nil {
				return err
			}
			op = "edit"
		}
	}

	data := map[string]interface{}{
		"page": "student.html",
		"app":  h.GlobalData["app"],
		"head": map[string]interface{}{
			"title": "Detail Siswa | Penentuan Penerimaan Beasiswa",
		},
		"leftside": h.GlobalData["leftside"],
		"topnav":   h.GlobalData["topnav"],
		"contents": map[string]interface{}{
			"op":      op,
			"student": student,
		},
		"user": h.GlobalData["user"],
		"assets": map[string]interface{}{
			"js": []string{
				"/assets/js/student.js",
			},
		},
	}

	return c.Render(http.StatusOK, "main.html", data)
}

// API Group

// ShowStudents is a function for ...
func (h *Handler) ShowStudents(c echo.Context) (err error) {
	class := c.QueryParam("class")

	f := Filter{}
	if err = c.Bind(&f); err != nil {
		return
	}

	if err = c.Validate(f); err != nil {
		return
	}

	m := models.Model{}
	m.Init()
	s := m.DB.Model("Student")

	students := []*models.Student{}

	filters := []bson.M{
		bson.M{"deleted": false},
	}
	if class != "" {
		cls, _ := strconv.Atoi(class)
		filters = append(filters, bson.M{"class": cls})
	}
	err = s.Find(bson.M{"$and": filters}).Sort(f.SortFields...).Skip(f.Skip).Limit(f.Limit).Exec(&students)

	if err != nil {
		return
	}

	return c.JSON(http.StatusOK, students)
}

// DeleteStudent is a function for ...
func (h *Handler) DeleteStudent(c echo.Context) (err error) {
	id := c.Param("student_id")

	m := models.Model{}
	m.Init()
	p := m.DB.Model("Student")

	student := models.Student{}
	err = p.FindId(bson.ObjectIdHex(id)).Exec(&student)
	if err != nil {
		return
	}
	err = student.Delete()
	if err != nil {
		return
	}

	err = student.Save()
	if err != nil {
		return
	}

	return c.JSON(http.StatusOK, student)
}

// ShowStudent is a function for ...
func (h *Handler) ShowStudent(c echo.Context) (err error) {
	id := c.Param("student_id")

	m := models.Model{}
	m.Init()
	s := m.DB.Model("Student")

	student := models.Student{}

	filters := []bson.M{
		bson.M{"_id": bson.ObjectIdHex(id)},
		bson.M{"deleted": false},
	}

	err = s.FindOne(bson.M{"$and": filters}).Exec(&student)

	if err != nil {
		return
	}

	return c.JSON(http.StatusOK, student)
}

// CreateStudent is a function for ...
func (h *Handler) CreateStudent(c echo.Context) (err error) {

	student := models.Student{}
	if err = c.Bind(&student); err != nil {
		return
	}

	if err = c.Validate(student); err != nil {
		return
	}
	// student.CreatedAt = student.Id.Time()

	m := models.Model{}
	m.Init()
	s := m.DB.Model("Student")

	s.New(&student)

	if err = student.Save(); err != nil {
		return
	}

	return
}

// UpdateStudentInfo is a function for ...
func (h *Handler) UpdateStudentInfo(c echo.Context) (err error) {

	studentInput := models.Student{}
	if err = c.Bind(&studentInput); err != nil {
		return
	}

	if err = c.Validate(studentInput); err != nil {
		return
	}

	m := models.Model{}
	m.Init()
	s := m.DB.Model("Student")
	student := models.Student{}
	err = s.FindId(studentInput.Id).Exec(&student)
	if err != nil {
		return
	}
	student.Name = studentInput.Name
	student.Class = studentInput.Class
	student.ClassGroup = studentInput.ClassGroup
	student.Gender = studentInput.Gender
	student.Birthcity = studentInput.Birthcity
	student.Birthday = studentInput.Birthday
	student.ParentInfo = studentInput.ParentInfo

	fmt.Println(student)

	if err = student.Save(); err != nil {
		return
	}
	return
}

// UpdateStudentInput is a function for ...
func (h *Handler) UpdateStudentInput(c echo.Context) (err error) {
	id := c.Param("student_id")
	input := models.Input{}
	if err = c.Bind(&input); err != nil {
		return
	}

	if err = c.Validate(input); err != nil {
		return
	}

	m := models.Model{}
	m.Init()
	s := m.DB.Model("Student")
	student := models.Student{}
	err = s.FindId(bson.ObjectIdHex(id)).Exec(&student)
	if err != nil {
		return
	}
	student.Input = input
	if err = student.Save(); err != nil {
		return
	}
	return
}
