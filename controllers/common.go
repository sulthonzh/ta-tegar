package controllers

type (
	// Handler is a struct for
	Handler struct {
		GlobalData map[string]interface{}
	}
	// Filter is a struct for
	Filter struct {
		Skip       int      `json:"skip" xml:"skip"`
		Limit      int      `json:"limit" xml:"limit"`
		SortFields []string `json:"sortFields" xml:"sortFields"`
	}
)

const (
	// StatusCreatedMessage stored data successfully
	StatusCreatedMessage = "Successfully"
)

// New is common function init
func (h *Handler) New() {
	h.GlobalData = map[string]interface{}{
		"user": map[string]interface{}{
			"isLogin":      false,
			"name":         "Admin",
			"url":          "/user",
			"photoProfile": "/assets/images/user.png",
		},
		"app": map[string]interface{}{
			"name": "TA - AHP",
			"icon": "university",
			"logo": map[string]interface{}{
				"default": "logo.png",
			},
		},
		"leftside": map[string]interface{}{
			"menus": []map[string]interface{}{
				{
					"group": "Umum",
					"childs": []map[string]interface{}{
						{
							"label": "Dashboard",
							"url":   "/",
							"icon":  "home",
						},
						{
							"label": "Siswa",
							"url":   "/student",
							"icon":  "user",
						},
						{
							"label": "Beasiswa",
							"url":   "/scholarship",
							"icon":  "money",
						},
						{
							"label": "Demo",
							"url":   "/demo",
							"icon":  "calculator",
						},
					},
				},
			},
		},
	}
}
