package controllers

import "testing"

func TestCreateStudent4(t *testing.T) {
	url := "http://localhost:9090/api/v1/student"

	tests := []TestStruct{
		// {`{}`, BadRequestCode, "", 0},
		// {`{"name":""}`, BadRequestCode, "", 0},
		// {`{"name": "Adam Ramadhan", "class": "1A"}`, SuccessRequestCode, "", 0},
		// {`{"name": "Ahmad David", "class": "1A"}`, SuccessRequestCode, "", 0},
		// {`{"name": "Arif Setia", "class": "1A"}`, SuccessRequestCode, "", 0},
		// {`{"name": "Athiyyah",  "class": "1A"}`, SuccessRequestCode, "", 0},
		// {`{"name": "Dandi ",  "class": "1A"}`, SuccessRequestCode, "", 0},
		// {`{"name": "David",  "class": "1A"}`, SuccessRequestCode, "", 0},
		// {`{"name": "Deni",  "class": "1A"}`, SuccessRequestCode, "", 0},
		// {`{"name": "Ariek",  "class": "1A"}`, SuccessRequestCode, "", 0},
		// {`{"name": "Fahrizal",  "class": "1A"}`, SuccessRequestCode, "", 0},

		{`{
			
	"name": "AFIF BAGAS PRATAMA",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "JAKARTA",
	"birthday": "2005-08-21T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "ENAS PRASTIYO",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "AGNIA SINTA MARGARETA",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2006-03-19T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "SAMAR WIDIANTO",
				"profession": 8
		  }
	}
	,"input" : {
        "gaji" : 2,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "AHMAD SYADZILI ALWI AZIZ",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2005-08-05T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "PRAYITNO",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 3,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "AHMAD ZIDHAN AL ASROH",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2004-06-15T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "NGADIONO",
				"profession": 8
		  }
	}
	,"input" : {
        "gaji" : 2,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "AHMADA ANANDIRA MATIN",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2005-12-23T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "RUDIK EKOPUTRO",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 3
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "AKHMAD THARIQ AFTAB SYAH",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2005-11-21T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "AKHMAD NIDHOM",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "ALVIAN TRI SAPUTRA",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2005-08-09T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "SUKIMAN",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "ANISA DINA NUR FADILLAH",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2005-11-16T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "Alm. KOTIM ADI GUNA",
				"profession": 7
		  }
	}
	,"input" : {
        "gaji" : 3,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 3
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "ARIVA HASNAH",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2006-12-12T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "NGADIONO",
				"profession": 4
		  }
	}
	,"input" : {
        "gaji" : 2,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 1
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "BINTANG FADILAH",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "PASURUAN",
	"birthday": "2006-01-10T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "SLAMET RIADI",
				"profession": 3
		  }
	}
	,"input" : {
        "gaji" : 3,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 1
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "CANTIKA AULIA PRAMANA",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2006-03-15T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "JANUAR PRAMANA",
				"profession": 6
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 3,
        "perilaku" : 3
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "CRISNA ARYA BAKTI S",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2003-10-19T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "MATAJI",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 1
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "DWI RISWANDA PRATIWI",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2006-05-10T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "SUWANDI",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "DWI SETIAWAN",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2005-06-02T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "SLAMET WAHYONO",
				"profession": 9
		  }
	}
	,"input" : {
        "gaji" : 2,
        "absensi" : 3,
        "nilai" : 3,
        "perilaku" : 1
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "FAIZAH ABELIA RUDIANTI",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2006-06-19T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "Rudi Susanto",
				"profession": 8
		  }
	}
	,"input" : {
        "gaji" : 2,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "IIN PUSPITA SARI",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2006-12-27T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "SUPARNO",
				"profession": 7
		  }
	}
	,"input" : {
        "gaji" : 3,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 1
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "KEISYA PRECIA LISDIAN TANEO",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "NTT",
	"birthday": "2006-03-12T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "YUNUS AGUNG J.A TANEO",
				"profession": 8
		  }
	}
	,"input" : {
        "gaji" : 2,
        "absensi" : 3,
        "nilai" : 3,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "KIKI DAHLIA",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2006-01-27T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "SA'AT",
				"profession":81
		  }
	}
	,"input" : {
        "gaji" : 2,
        "absensi" : 1,
        "nilai" : 2,
        "perilaku" : 1
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "LISA ARDIANTI RAHMAWATI",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2006-05-05T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "RUSIADI",
				"profession": 7
		  }
	}
	,"input" : {
        "gaji" : 3,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "M. PUTRA ZAHRUN NIZAF",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2005-03-01T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "ARIFIN",
				"profession": 8
		  }
	}
	,"input" : {
        "gaji" : 2,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 1
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "MAHARDIAN MEILINA SALSABILA",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "CILACAP",
	"birthday": "2006-05-18T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "Alm. MUHARIYADI",
				"profession": 0
		  }
	}
	,"input" : {
        "gaji" : 3,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "MEI ASTI RAHMAWATI",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2006-05-20T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "SUMIT",
				"profession":21
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 3,
        "perilaku" : 3
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "MUCH. KURNIAWAN SAPUTRA W",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2005-06-29T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "SUNARTO",
				"profession": 8
		  }
	}
	,"input" : {
        "gaji" : 2,
        "absensi" : 3,
        "nilai" : 3,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "MUCHAMMAD HINDARTO S",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2006-06-08T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "BAMBANG SUTRISNO",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "MUHAMAD RISKI ABADI",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2004-08-23T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "ROCHANI",
				"profession": 1
		  }
	}
	,"input" : {
        "gaji" : 2,
        "absensi" : 2,
        "nilai" : 2,
        "perilaku" : 1
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "MUHAMMAD FAHRIL SATRIA",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2005-09-28T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "RUMINTO",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 1
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "MUHAMMAD FEBRIAN AL IMRON",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2005-02-15T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "SUMARIONO",
				"profession": 8
		  }
	}
	,"input" : {
        "gaji" : 2,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 1
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "MUHAMMAD MISBAHUL HUDA",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2006-05-03T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "HADI SANTOSO",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "MUHAMMAD NIZAR HABIBI",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2006-05-11T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "SLAMET",
				"profession": 7
		  }
	}
	,"input" : {
        "gaji" : 3,
        "absensi" : 3,
        "nilai" : 3,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "MUHAMMAD SYAHRUL A",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2005-12-18T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "HARTONO",
				"profession": 8
		  }
	}
	,"input" : {
        "gaji" : 2,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "NADINE CAHYA NING AULIA",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2005-09-01T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "BUDI SAMPURNO",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 3
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "NAILA ZAFIROH",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2005-08-23T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "MOCHAMMAD SAIFUL",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 3
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "NIKE NURWAHYUNI",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2006-06-26T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "AGUS NURSIYO",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 3,
        "perilaku" : 1
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "NILAM OKTA BELLA",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2005-10-10T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "SUMARDI",
				"profession": 5
		  }
	}
	,"input" : {
        "gaji" : 2,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "OCA ANGGRAENI MEISYA PUTRI",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2006-05-30T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "NANANG PRAYITNO",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "REGITA INDAH PERMATA SARI",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2006-06-18T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "SUJARWO",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 1
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "RIDDHO DWI FIRMANSYAH",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2006-02-12T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "SUMARYONO",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 1
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "RIZQI RAMADHANI",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2005-10-29T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "SRIONO",
				"profession": 6
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "SALSABILA PUTRI TOASTAINI",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2006-04-26T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "MOCH UMAR",
				"profession": 2
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "SITI NUR AULIA ISMY",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2005-08-01T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "MISRIANTO",
				"profession": 1
		  }
	}
	,"input" : {
        "gaji" : 2,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 1
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "TRI AYUK YULIANTI",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2006-07-11T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "SUTRISNO",
				"profession": 6
		  }
	}
	,"input" : {
        "gaji" : 1,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 3
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "UBAIDDILLAH",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2004-05-24T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "MOCH RIZKY",
				"profession": 8
		  }
	}
	,"input" : {
        "gaji" : 2,
        "absensi" : 1,
        "nilai" : 2,
        "perilaku" : 1
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "VRISCA AVRILIA PUTRI",
	"class": 4,
	"classGroup": "",
	"gender": 2,
	"birthcity": "MALANG",
	"birthday": "2005-04-06T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "BEJO ARIFIN",
				"profession": 4
		  }
	}
	,"input" : {
        "gaji" : 3,
        "absensi" : 3,
        "nilai" : 2,
        "perilaku" : 2
    }
}`, SuccessRequestCode, "", 0},
		{`{
			
	"name": "YOGA WAHYU SAPUTRA",
	"class": 4,
	"classGroup": "",
	"gender": 1,
	"birthcity": "MALANG",
	"birthday": "2006-03-03T00:00:00.000Z",
	"parentInfo": {
		  "father": {
				"name": "MULYONO",
				"profession": 1
		  }
	}
	,"input" : {
        "gaji" : 2,
        "absensi" : 2,
        "nilai" : 2,
        "perilaku" : 1
    }
}`, SuccessRequestCode, "", 0},
	}

	DoTest(url, tests, t)
	DisplayTestCaseResults("CreateStudent", tests, t)
}

func TestCreateStudent5(t *testing.T) {
	url := "http://localhost:9090/api/v1/student"

	tests := []TestStruct{
		// {`{}`, BadRequestCode, "", 0},
		// {`{"name":""}`, BadRequestCode, "", 0},
		// {`{"name": "Adam Ramadhan", "class": "1A"}`, SuccessRequestCode, "", 0},
		// {`{"name": "Ahmad David", "class": "1A"}`, SuccessRequestCode, "", 0},
		// {`{"name": "Arif Setia", "class": "1A"}`, SuccessRequestCode, "", 0},
		// {`{"name": "Athiyyah",  "class": "1A"}`, SuccessRequestCode, "", 0},
		// {`{"name": "Dandi ",  "class": "1A"}`, SuccessRequestCode, "", 0},
		// {`{"name": "David",  "class": "1A"}`, SuccessRequestCode, "", 0},
		// {`{"name": "Deni",  "class": "1A"}`, SuccessRequestCode, "", 0},
		// {`{"name": "Ariek",  "class": "1A"}`, SuccessRequestCode, "", 0},
		// {`{"name": "Fahrizal",  "class": "1A"}`, SuccessRequestCode, "", 0},

		{`{
			"name": "SHANDIKA DAFFA FARDIANSYAH",
			"class": 5,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2002-01-09T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "IMAM CHAMBALI",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 1
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "ARLIN KURNIAWAN",
			"class": 5,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-02-11T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "ARIFIN",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 1
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MUHAMMAD ANGGA PRADANA",
			"class": 5,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2002-10-27T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "ATIM",
						"profession": 3
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 2,
				"nilai" : 3,
				"perilaku" : 1
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "RAMADHAN BUDI SAPUTRA",
			"class": 5,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-11-19T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "AGUS BUDI PRIYANTO",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 1
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "RISKA INDAHSARI",
			"class": 5,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-09-22T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "ISHARIYANTO",
						"profession": 10
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "KHOFIFAH NUR HALIZA",
			"class": 5,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-05-06T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SUGIONO",
						"profession": 5
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "ADAM RAGHEB WIJAYA",
			"class": 5,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2004-01-19T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "YUSUF LEKAMANTRI",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 1
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "AGUNG SURYA BAWONO",
			"class": 5,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2005-04-12T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "ARIS JUNAEDI",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 1
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "ACHMAD FAKHRU ROZI",
			"class": 5,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2004-03-24T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SAIB",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 1
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "ARIEL CHARISTEAS",
			"class": 5,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2004-11-09T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "BUDI PRIHATIN",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "DEVITA AMALIA SUKMA",
			"class": 5,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-12-20T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "MOCH. SUHARTOYO",
						"profession": 6
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 3
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "DIA AYU ARTA",
			"class": 5,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-03-03T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "HADI PRESTIWANTO",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "DIAN HARIANA RAMADANTI",
			"class": 5,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-11-10T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "HARIYANTO",
						"profession": 4
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "FERLY DWI ANDINI",
			"class": 5,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-08-28T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "ELY KURNIAWAN",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "GILANG SAPUTRA",
			"class": 5,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2004-06-28T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "EDY SLAMET",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "LINDA SARASTIA EFENDI",
			"class": 5,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2005-03-05T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "ALIMUDIN ALM",
						"profession": 7
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "LINGGA LUKI NURHIDAYAT",
			"class": 5,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2005-02-19T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "ABD GOFAR",
						"profession": 9
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 1
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "LINTANG WAHYU SATRIO",
			"class": 5,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2005-02-05T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "HENDRO UTOYO",
						"profession":82
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 1
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MUHAMAD FIRMAN ERFANI AZHAR",
			"class": 5,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-05-29T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "PARJIYO",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MUHAMAD IQBAL SURYA PRATAMA",
			"class": 5,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2005-06-21T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "PUJI DARUNGKE",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MUHAMMAD LABIB AZIZ DZUIFFAH",
			"class": 5,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2004-10-09T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "FADKHULIL IBADIK",
						"profession": 3
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MUHAMAD PRADITYA SAPUTRA",
			"class": 5,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2005-11-17T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SULISTYONO",
						"profession":28
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MOHAMMAD WAHYU RIZKI ADI",
			"class": 5,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2004-07-27T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SUMADI",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 1
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MARSELA ERLIN PUSPITA DEWI",
			"class": 5,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2005-03-07T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "USMAN AFANDY",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MOCH RIZKY ANDRYANSYAH",
			"class": 5,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2005-05-27T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "NGATIMIN",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MUHAMAD RIDWAN",
			"class": 5,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2004-08-28T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "NASUKI",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MUHAMMAD MUFLIH",
			"class": 5,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2004-03-10T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "MUKARAP",
						"profession": 4
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "NUR AZIZAH SELVIA SARI",
			"class": 5,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-12-05T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "MASNURHADI",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 3
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "NUR ILHAM ADHA",
			"class": 5,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2005-01-19T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "MISTANI",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "OLIVE KYLA MELANDRE",
			"class": 5,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2005-07-09T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "M. ARIF ISWANTORO",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "PUTRI DESYANA LESTARI",
			"class": 5,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-12-14T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "RUMAJI",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "REINA SASMITA PUTRI",
			"class": 5,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-06-07T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "PUPUT PUJI SASMITO",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "RINJANI OKTAVIA",
			"class": 5,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-10-10T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "DASUKI",
						"profession": 3
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "RIVALDI PUTRA TARINDA",
			"class": 5,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2005-08-19T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "MUHTAR",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "RIZKI DWI AROHMAN",
			"class": 5,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2005-05-10T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "FATCHUR ROCHMAN",
						"profession": 3
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "SATRIA RACHMADANDI",
			"class": 5,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2005-01-07T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SOFI PAMUNGKAS",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "TARISTA RISKY AMELIA",
			"class": 5,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2005-12-02T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "DWI AGUNG PRASETYO",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 3
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "NIKEN AYU ANANDA",
			"class": 5,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-07-27T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "MUJIONO",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "IRJIQ FITRAH ALDIANSYAH",
			"class": 5,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2005-10-30T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "ABU BAKAR",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "PUTRI AYU FIRNANDA NABILA",
			"class": 5,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2005-03-04T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SAMSUL ANAM",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MOCH. SAIFUL FADIL ABDULLAH",
			"class": 5,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2005-04-28T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "MUHAMMAD SOIM",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MOCHAMAD ADEN RENAN",
			"class": 5,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2005-07-23T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "MOHAMAD PARIS",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "AFNI NUR FATMARADEWI",
			"class": 5,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "CILACAP",
			"birthday": "2005-03-30T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "Ijud Artanto",
						"profession": 3
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MOCH. NAZHIF DZAKI MAULANA",
			"class": 5,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "PASURUAN",
			"birthday": "2004-04-26T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SAMSUL MA'ARIF",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "FAIZA RIFQY ARDHIAN PRATAMA",
			"class": 5,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "BANYUWANGI",
			"birthday": "2004-09-24T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "ENDRO DWI CAHYANTO",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
	}

	DoTest(url, tests, t)
	DisplayTestCaseResults("CreateStudent", tests, t)
}
func TestCreateStudent6(t *testing.T) {
	url := "http://localhost:9090/api/v1/student"

	tests := []TestStruct{
		{`{
			"name": "ADAM RAMADHAN NUR ABDIANSYAH",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2001-11-17T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SLAMET HARIYANTO",
						"profession": 1
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 1
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "AHMAD DAVID SAPUTRA",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-12-14T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "NGADIONO",
						"profession": 7
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "ALIFA RUSTANTIA WERDI NINGSIH",
			"class": 6,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-02-20T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SUPRIYADI",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "ARIF SETIA PUTRA",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-08-22T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "JUMADI",
						"profession": 4
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "ATHIYYAH RISTI MAULINA",
			"class": 6,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-05-09T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SUMARNO ARIES",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "DANDI PRASETIYO",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-07-01T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SIYONO",
						"profession": 5
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 1
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "DAVID SAPUTRA MULYANTO",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-09-23T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "RUDI MULYANTO",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "DENI SAPUTRO",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-12-01T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "MISTO",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "ERIEK REVALDO",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2004-05-22T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "WINARNO",
						"profession": 1
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "FAHRIZAL BAYU PRASETYO",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2004-05-30T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "MUHAMMAD SUHASTO",
						"profession": 9
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "FAIZATUS SYIFA",
			"class": 6,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-08-15T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SAMURI",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 3
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "FEBY FARISYA RENANDA",
			"class": 6,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2003-11-29T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "MOHAMAD PARIS",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "FIKKY MUHAMMAD ILHAM",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-09-02T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "IMRON ROSYADI",
						"profession": 3
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "HESTI DWI KURNIA SUDARMINTO",
			"class": 6,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-03-10T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "EKO SUDARMINTO",
						"profession": 3
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "ICHA APRILIA SUBEKTI",
			"class": 6,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-04-07T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "HERMAN SUBEKTI",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "IRFAN SARONI",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-11-18T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "KUSDI",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "JIHAN AYU CAHYANINGTYAS",
			"class": 6,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2003-09-19T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "GIANTO",
						"profession": 1
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 3
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "M. DZUL JALALI WAL IKROM",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-11-25T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "MUHAMMAD SOFUAN",
						"profession":8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 3
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MOCHAMMAD FERDIAN SAPUTRA",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-09-24T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "AINUR ROFIQ",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 3
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MOCHAMMAD RIZAL ZIBRAN",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-08-14T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "JUMANAN",
						"profession": 1
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 1,
				"nilai" : 2,
				"perilaku" : 1
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MUHAMMAD AQSHA",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2004-03-06T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "EDI SISWANTO",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "NUR 'AINI KHUSNUL KHOTIMAH",
			"class": 6,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2003-11-07T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "HERI KURNIAWAN",
						"profession":1
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "NURANISYAH",
			"class": 6,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2003-07-09T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "MUHAMAD JAINURI",
						"profession": 10
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 3
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "OKTAVIANA DEWIANJAR PUSPITASARI",
			"class": 6,
			"classGroup": "A",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2002-10-28T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SUPRIYADI",
						"profession": 7
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 3
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "PUTRA PURNAMA",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2004-05-13T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "JUMAIN",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "RENDI MUHAMAD HAFANDI",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-03-09T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "IMAM HANAFI",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 2,
				"nilai" : 2,
				"perilaku" : 1
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "RISZKY TRI WAHYUDIANTO",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2004-04-06T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "HARIYANTO ALM",
						"profession": 3
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "RIZKY OKTAFIANDI",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-10-20T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "UDIK SETIAWAN",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "SETYO PAMUJO",
			"class": 6,
			"classGroup": "A",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2004-08-01T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "Alm. AGUS PURNOMO",
						"profession": 7
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 3
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "AGFAN FIQH WILDANZAN",
			"class": 6,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "B.LAMPUNG",
			"birthday": "2004-08-09T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "ABU AMAR",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 3
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "AGUNG TRI ANUGRAH",
			"class": 6,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-12-15T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "AKHMAD SYARIF",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "AHMAD ALI ZAKI AL MUHTAR",
			"class": 6,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2004-06-26T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SHOLICHIN",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "ANZUA VINDI AJENG",
			"class": 6,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2003-04-03T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "ISWAHYUDI",
						"profession": 1
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "ARDANA DWI SAPTA",
			"class": 6,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-09-03T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SUGIARTO",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 3
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "DELLA PUSPITA AGUSTINA",
			"class": 6,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2003-08-01T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "PURWANTO",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 3
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "DHEA AYU FIRNANDA",
			"class": 6,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2003-12-08T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SUTRISNO ADI",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "FADILLA",
			"class": 6,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "JAKARTA",
			"birthday": "2003-04-12T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "YUDI PRIYADI",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "FATHAN ATHILLAH MUAZIZ",
			"class": 6,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2005-01-02T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SAIFUD AZIZ",
						"profession": 3
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "INDAH SEPTYA SARI",
			"class": 6,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2003-09-18T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SURADI",
						"profession": 1
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "INTAN CATUR MEILASARI",
			"class": 6,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2002-05-14T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "AGUS SUMIRAN",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "LISA DIAN AYU LESTARI",
			"class": 6,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2001-04-27T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "MOHAMAD SA'I",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "M. FAUZI ARISONA",
			"class": 6,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2004-02-17T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "Alm. BUHARI",
						"profession": 7
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MARHANA FARIZA IMTINAN",
			"class": 6,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "CILACAP",
			"birthday": "2003-09-26T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "Alm. Muhariyadi",
						"profession": 7
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 3
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MUCHAMMAD ISWAHYU HIDAYAT",
			"class": 6,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-09-30T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SUHADAK",
						"profession": 1
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MUHAMMAD CHOIRUNNAS",
			"class": 6,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-08-25T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SULIYADI",
						"profession": 8
				  }
			},
			"input" : {
				"gaji" : 2,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 3
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MUHAMMAD DONI SETIAWAN",
			"class": 6,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2002-03-15T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SUPAKIH",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 1
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "MUHAMMAD YUSUF PRASETYO",
			"class": 6,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2001-09-15T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "AGUS SALIM",
						"profession": 4
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 1
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "NARESWARI TARAFINDITA",
			"class": 6,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2004-01-31T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "AGUS SULIANTO",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "RENI KARTINA AMELIA",
			"class": 6,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "SIDOARJO",
			"birthday": "2004-02-10T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SUKARTONO",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "RIFKY EKA PRASETYA",
			"class": 6,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-11-22T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "AGUS UBAIDILAH",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "SHOUFIL ISLAMI",
			"class": 6,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2003-05-13T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "WIRYONO",
						"profession": 2
				  }
			},
			"input" : {
				"gaji" : 1,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 3
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "SINDY LESTARI",
			"class": 6,
			"classGroup": "B",
			"gender": 2,
			"birthcity": "MALANG",
			"birthday": "2003-04-24T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SUMARLAN",
						"profession": 1
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 2,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
		{`{
			"name": "WILDAN ARIF RAMADHAN",
			"class": 6,
			"classGroup": "B",
			"gender": 1,
			"birthcity": "MALANG",
			"birthday": "2003-11-21T00:00:00.000Z",
			"parentInfo": {
				  "father": {
						"name": "SAMSUL ARIF",
						"profession": 3
				  }
			},
			"input" : {
				"gaji" : 3,
				"absensi" : 3,
				"nilai" : 3,
				"perilaku" : 2
			}
		}`, SuccessRequestCode, "", 0},
	}

	DoTest(url, tests, t)
	DisplayTestCaseResults("CreateStudent", tests, t)
}
