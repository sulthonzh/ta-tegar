package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"strconv"
	"strings"

	"gopkg.in/mgo.v2/bson"
	resty "gopkg.in/resty.v0"

	"gitlab.com/sulthonzh/tegar/middleware/algorithm"
	"gitlab.com/sulthonzh/tegar/models"

	"github.com/labstack/echo"
)

// PageScholarships is a function for ...
func (h *Handler) PageScholarships(c echo.Context) (err error) {

	scholarships := []*models.Scholarship{}
	resp, err := resty.R().
		SetMultiValueQueryParams(c.QueryParams()).
		SetHeader("Accept", "application/json").
		Get("http://127.0.0.1:9090/api/v1/scholarship")

	if resp.StatusCode() == 200 {
		err = json.Unmarshal(resp.Body(), &scholarships)

		if err != nil {
			return
		}
	}

	data := map[string]interface{}{
		"page": "scholarships.html",
		"app":  h.GlobalData["app"],
		"head": map[string]interface{}{
			"title": "Beasiswa | Penentuan Penerimaan Beasiswa",
		},
		"leftside": h.GlobalData["leftside"],
		"topnav":   h.GlobalData["topnav"],
		"contents": map[string]interface{}{
			"scholarships": scholarships,
		},
		"user": h.GlobalData["user"],
		"assets": map[string]interface{}{
			"js": []string{
				"/assets/js/scholarships.js",
			},
		},
	}
	return c.Render(http.StatusOK, "main.html", data)
}

// PageScholarship is a function for ...
func (h *Handler) PageScholarship(c echo.Context) (err error) {
	scholarshipID := c.Param("scholarship_id")

	op := ""
	scholarship := models.Scholarship{}
	participants := []*models.Participant{}
	participantsModify := []interface{}{}

	if scholarshipID == "create" {
		op = "create"
		scholarship.Id = bson.NewObjectId()
		scholarship.CreatedAt = scholarship.Id.Time()
	} else {

		respS, err := resty.R().
			SetMultiValueQueryParams(c.QueryParams()).
			SetHeader("Accept", "application/json").
			Get("http://127.0.0.1:9090/api/v1/scholarship/" + scholarshipID)

		if respS.StatusCode() == 200 {
			err = json.Unmarshal(respS.Body(), &scholarship)

			if err != nil {
				return err
			}
		}
		op = "edit"

		respP, err := resty.R().
			SetMultiValueQueryParams(c.QueryParams()).
			SetHeader("Accept", "application/json").
			Get("http://127.0.0.1:9090/api/v1/scholarship/" + scholarshipID + "/participant")

		if respP.StatusCode() == 200 {
			err = json.Unmarshal(respP.Body(), &participants)

			if err != nil {
				return err
			}

			for _, p := range participants {
				temp := map[string]interface{}{}
				temp["id"] = p.Id.Hex()
				temp["output"] = p.Output
				temp["student"] = p.Student

				s := reflect.ValueOf(&p.Input).Elem()
				typeOfT := s.Type()
				input := map[string]string{}
				for i := 0; i < s.NumField(); i++ {
					f := s.Field(i)
					for _, param := range scholarship.Parameters.([]interface{}) {
						tempParam := param.(map[string]interface{})
						if strings.ToLower(tempParam["name"].(string)) == strings.ToLower(typeOfT.Field(i).Name) {

							for _, cri := range tempParam["criteria"].([]interface{}) {
								criTemp := cri.(map[string]interface{})

								if criTemp["value"].(float64) == float64(f.Interface().(int)) {
									input[strings.ToLower(tempParam["name"].(string))] = criTemp["condition"].(string)
								}
							}
						}
					}
					// fmt.Printf("%d: %s %s = %v\n", i, typeOfT.Field(i).Name, f.Type(), f.Interface())
				}
				temp["input"] = input

				participantsModify = append(participantsModify, temp)
			}
			println(participantsModify)
		}
	}

	data := map[string]interface{}{
		"page": "scholarship.html",
		"app":  h.GlobalData["app"],
		"head": map[string]interface{}{
			"title": "Beasiswa | Peserta | Penentuan Penerimaan Beasiswa",
		},
		"leftside": h.GlobalData["leftside"],
		"topnav":   h.GlobalData["topnav"],
		"contents": map[string]interface{}{
			"op":           op,
			"scholarship":  scholarship,
			"participants": participantsModify,
		},
		"user": h.GlobalData["user"],
		"assets": map[string]interface{}{
			"js": []string{
				"/assets/js/scholarship.js",
				"/assets/vendors/datatables.net/js/jquery.dataTables.min.js",
				"/assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js",
				"/assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js",
				"/assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js",
				"/assets/vendors/datatables.net-buttons/js/buttons.flash.min.js",
				"/assets/vendors/datatables.net-buttons/js/buttons.html5.min.js",
				"/assets/vendors/datatables.net-buttons/js/buttons.print.min.js",
				"/assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js",
				"/assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js",
				"/assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js",
				"/assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js",
				"/assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js",
				"/assets/vendors/jszip/dist/jszip.min.js",
				"/assets/vendors/pdfmake/build/pdfmake.min.js",
				"/assets/vendors/pdfmake/build/vfs_fonts.js",
			},
		},
		"identity": map[string]interface{}{
			"scholarshipId": scholarshipID,
		},
	}
	return c.Render(http.StatusOK, "main.html", data)
}

// PageParticipant is a function for ...
func (h *Handler) PageParticipant(c echo.Context) (err error) {
	scholarshipID := c.Param("scholarship_id")
	participantID := c.Param("participant_id")

	op := ""
	scholarship := models.Scholarship{}
	participant := models.Participant{}

	vo := map[string]map[int]string{}

	if scholarshipID != "" {

		respS, err := resty.R().
			SetMultiValueQueryParams(c.QueryParams()).
			SetHeader("Accept", "application/json").
			Get("http://127.0.0.1:9090/api/v1/scholarship/" + scholarshipID)

		if respS.StatusCode() == 200 {
			err = json.Unmarshal(respS.Body(), &scholarship)

			if err != nil {
				return err
			}
		}

		for _, parameter := range scholarship.Parameters.([]interface{}) {
			dataTemp := parameter.(map[string]interface{})

			if dataTemp["type"] == "satuan criteria" {
				voTemp := map[int]string{}
				for _, criteria := range dataTemp["criteria"].([]interface{}) {
					criteriaTemp := criteria.(map[string]interface{})
					voTemp[int(criteriaTemp["value"].(float64))] = criteriaTemp["condition"].(string)
				}

				vo[dataTemp["name"].(string)] = voTemp
			}

		}
	}

	if participantID == "create" {
		op = "create"
		participant.Id = bson.NewObjectId()
		participant.CreatedAt = participant.Id.Time()
	} else {
		resp, err := resty.R().
			SetMultiValueQueryParams(c.QueryParams()).
			SetHeader("Accept", "application/json").
			Get("http://127.0.0.1:9090/api/v1/participant/" + participantID)

		if resp.StatusCode() == 200 {
			err = json.Unmarshal(resp.Body(), &participant)

			if err != nil {
				return err
			}
			op = "edit"
		}

	}

	data := map[string]interface{}{
		"page": "participant.html",
		"app":  h.GlobalData["app"],
		"head": map[string]interface{}{
			"title": "Beasiswa | Peserta | Penentuan Penerimaan Beasiswa",
		},
		"leftside": h.GlobalData["leftside"],
		"topnav":   h.GlobalData["topnav"],
		"contents": map[string]interface{}{
			"op":       op,
			"criteria": scholarship.Parameters.([]interface{})[0].(map[string]interface{})["criteria"],
			"vo":       vo,
			// "vo": map[string]map[int]string{
			// 	"gaji":     {1: ">2.000.000", 2: "1.000.000-2.000.000", 3: "0-1.000.000"},
			// 	"absensi":  {1: ">6", 2: "3-5", 3: "0-2"},
			// 	"nilai":    {1: "61-70", 2: "71-80", 3: "81-90"},
			// 	"perilaku": {1: "C", 2: "B", 3: "A"},
			// },
			"participant": participant,
		},
		"user": h.GlobalData["user"],
		"assets": map[string]interface{}{
			"js": []string{
				"/assets/js/participant.js",
			},
		},
		"identity": map[string]interface{}{
			"scholarshipId": scholarshipID,
			"participantId": participantID,
		},
	}
	fmt.Print(scholarship.Parameters.([]interface{})[0].(map[string]interface{})["criteria"])
	return c.Render(http.StatusOK, "main.html", data)
}

// PageCriteria is a function for ...
func (h *Handler) PageCriteria(c echo.Context) (err error) {
	scholarshipID := c.Param("scholarship_id")
	strCriteriaIndex := c.Param("index")

	op := ""
	scholarship := models.Scholarship{}
	respS, err := resty.R().
		SetMultiValueQueryParams(c.QueryParams()).
		SetHeader("Accept", "application/json").
		Get("http://127.0.0.1:9090/api/v1/scholarship/" + scholarshipID)

	if respS.StatusCode() == 200 {
		err = json.Unmarshal(respS.Body(), &scholarship)

		if err != nil {
			return err
		}

	}

	parameter := map[string]interface{}{}
	criteriaIndex := 0
	if strCriteriaIndex == "create" {
		op = "create"

	} else {
		criteriaIndex, _ = strconv.Atoi(strCriteriaIndex)
		parameter = scholarship.Parameters.([]interface{})[criteriaIndex].(map[string]interface{})
		op = "edit"

	}

	data := map[string]interface{}{
		"page": "criteria.html",
		"app":  h.GlobalData["app"],
		"head": map[string]interface{}{
			"title": "Beasiswa | Criteria | Penentuan Penerimaan Beasiswa",
		},
		"leftside": h.GlobalData["leftside"],
		"topnav":   h.GlobalData["topnav"],
		"contents": map[string]interface{}{
			"op":        op,
			"index":     criteriaIndex,
			"parameter": parameter,
		},
		"user": h.GlobalData["user"],
		"assets": map[string]interface{}{
			"js": []string{
				"/assets/js/criteria.js",
			},
		},
		"identity": map[string]interface{}{
			"scholarshipId": scholarshipID,
			"criteriaIndex": criteriaIndex,
		},
	}
	return c.Render(http.StatusOK, "main.html", data)
}

// PageDemo is a function for ...
func (h *Handler) PageDemo(c echo.Context) (err error) {

	scholarships := []models.Scholarship{}

	respS, err := resty.R().
		SetMultiValueQueryParams(c.QueryParams()).
		SetHeader("Accept", "application/json").
		Get("http://127.0.0.1:9090/api/v1/scholarship")

	if respS.StatusCode() == 200 {
		err = json.Unmarshal(respS.Body(), &scholarships)

		if err != nil {
			return err
		}
	}

	data := map[string]interface{}{
		"page": "pengujian.html",
		"app":  h.GlobalData["app"],
		"head": map[string]interface{}{
			"title": "Beasiswa | Criteria | Penentuan Penerimaan Beasiswa",
		},
		"leftside": h.GlobalData["leftside"],
		"topnav":   h.GlobalData["topnav"],
		"contents": map[string]interface{}{
			"scholarships": scholarships,
		},
		"user": h.GlobalData["user"],
		"assets": map[string]interface{}{
			"js": []string{
				"/assets/js/demo.js",
				"/assets/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js",
			},
		},
		"identity": map[string]interface{}{},
	}
	return c.Render(http.StatusOK, "main.html", data)
}

// API Group

// CreateScholarship is a function for ...
func (h *Handler) CreateScholarship(c echo.Context) (err error) {
	scholarship := models.Scholarship{}
	if err = c.Bind(&scholarship); err != nil {
		return
	}

	if err = c.Validate(scholarship); err != nil {
		return
	}

	parameters := `[{"criteria":[{"condition":"","name":"gaji","value":0},{"condition":"","name":"absensi","value":0},{"condition":"","name":"nilai","value":0},{"condition":"","name":"perilaku","value":0}],"name":"criteria","parent":"root","questionnaire":[{"identitas":2,"n1":"gaji","n2":"absensi"},{"identitas":3,"n1":"gaji","n2":"nilai"},{"identitas":3,"n1":"gaji","n2":"perilaku"},{"identitas":2,"n1":"absensi","n2":"nilai"},{"identitas":3,"n1":"absensi","n2":"perilaku"},{"identitas":2,"n1":"nilai","n2":"perilaku"}],"type":"criteria"},{"criteria":[{"condition":"","name":"sangat baik","value":3},{"condition":"","name":"baik","value":2},{"condition":"","name":"cukup","value":1}],"name":"sub criteria","parent":"criteria","questionnaire":[{"identitas":2,"n1":"sangat baik","n2":"baik"},{"identitas":3,"n1":"sangat baik","n2":"cukup"},{"identitas":2,"n1":"baik","n2":"cukup"}],"type":"criteria"},{"criteria":[{"condition":"0-1.000.000","name":"sangat baik","value":3},{"condition":"1.000.001-2.000.000","name":"baik","value":2},{"condition":"2.000.001-","name":"cukup","value":1}],"name":"gaji","parent":"root","questionnaire":[{"identitas":3,"n1":"sangat baik","n2":"baik"},{"identitas":3,"n1":"sangat baik","n2":"cukup"},{"identitas":3,"n1":"baik","n2":"cukup"}],"type":"satuan criteria"},{"criteria":[{"condition":"0-2","name":"sangat baik","value":3},{"condition":"3-5","name":"baik","value":2},{"condition":"6-","name":"cukup","value":1}],"name":"absensi","parent":"root","questionnaire":[{"identitas":3,"n1":"sangat baik","n2":"baik"},{"identitas":2,"n1":"sangat baik","n2":"cukup"},{"identitas":3,"n1":"baik","n2":"cukup"}],"type":"satuan criteria"},{"criteria":[{"condition":"81-90","name":"sangat baik","value":3},{"condition":"71-80","name":"baik","value":2},{"condition":"61-70","name":"cukup","value":1}],"name":"nilai","parent":"root","questionnaire":[{"identitas":2,"n1":"sangat baik","n2":"baik"},{"identitas":2,"n1":"sangat baik","n2":"cukup"},{"identitas":3,"n1":"baik","n2":"cukup"}],"type":"satuan criteria"},{"criteria":[{"condition":"A","name":"sangat baik","value":3},{"condition":"B","name":"baik","value":2},{"condition":"C","name":"cukup","value":1}],"name":"perilaku","parent":"root","questionnaire":[{"identitas":2,"n1":"sangat baik","n2":"baik"},{"identitas":3,"n1":"sangat baik","n2":"cukup"},{"identitas":2,"n1":"baik","n2":"cukup"}],"type":"satuan criteria"}]`
	parametersByte := []byte(parameters)

	pr := []algorithm.Parameter{}

	json.Unmarshal(parametersByte, &pr)
	scholarship.Parameters = pr

	m := models.Model{}
	m.Init()
	s := m.DB.Model("Scholarship")

	s.New(&scholarship)

	if err = scholarship.Save(); err != nil {
		return
	}

	return
}

// PatchScholarship is a function for ...
func (h *Handler) PatchScholarship(c echo.Context) (err error) {
	scholarship := models.Scholarship{}
	if err = c.Bind(&scholarship); err != nil {
		return
	}

	if err = c.Validate(scholarship); err != nil {
		fmt.Println(err)
		return
	}

	m := models.Model{}
	m.Init()
	s := m.DB.Model("Scholarship")

	oldScholarship := models.Scholarship{}
	err = s.FindId(scholarship.Id).Exec(&oldScholarship)

	if scholarship.Type != "" {
		oldScholarship.Type = scholarship.Type
	}

	if scholarship.Metode != "" {
		oldScholarship.Metode = scholarship.Metode

	}

	if scholarship.Parameters != nil {
		oldScholarship.Parameters = scholarship.Parameters

	}

	if scholarship.Participants != nil {
		oldScholarship.Participants = scholarship.Participants

	}

	if oldScholarship.Save() != nil {
		return
	}

	return
}

// AddBulk is a function for ...
func (h *Handler) AddBulk(c echo.Context) (err error) {
	id := c.QueryParam("scholarship_id")
	class, _ := strconv.Atoi(c.QueryParam("class"))

	m := models.Model{}
	m.Init()

	println("id:", id)
	println("kelas:", class)

	st := m.DB.Model("Student")
	students := []*models.Student{}
	err = st.Find(bson.M{
		"$and": []bson.M{
			bson.M{"class": class},
			bson.M{"deleted": false},
		},
	}).Exec(&students)

	if err != nil {
		return
	}

	participants := []models.Participant{}
	for _, s := range students {
		temp := models.Participant{}
		temp.Input = s.Input
		temp.Student = s.Id.Hex()
		participants = append(participants, temp)
	}

	// println("############", participants)

	err = h.participantBulk(c, id, participants)

	if err != nil {
		return
	}

	return
}

// AddData is a function for ...
func (h *Handler) AddData(c echo.Context) (err error) {
	id := c.Param("scholarship_id")

	parameters := []algorithm.Parameter{}
	if err = c.Bind(&parameters); err != nil {
		return
	}

	fmt.Println(parameters)

	m := models.Model{}
	m.Init()
	s := m.DB.Model("Scholarship")
	scholarship := models.Scholarship{}
	err = s.FindId(bson.ObjectIdHex(id)).Exec(&scholarship)

	if err != nil {
		return
	}

	scholarship.Parameters = parameters
	if err = scholarship.Save(); err != nil {
		return
	}

	return
}

// AddPaticipant is a function for ...
func (h *Handler) AddPaticipant(c echo.Context) (err error) {
	id := c.Param("scholarship_id")

	participant := models.Participant{}
	if err = c.Bind(&participant); err != nil {
		return
	}

	m := models.Model{}
	m.Init()
	p := m.DB.Model("Participant")
	s := m.DB.Model("Scholarship")
	scholarship := models.Scholarship{}
	err = s.FindId(bson.ObjectIdHex(id)).Populate("Participants").Exec(&scholarship)

	if err != nil {
		return
	}

	if _, ok := scholarship.Participants.([]*models.Participant); !ok {
		fmt.Println("something went wrong during type assertion. wrong type?")
		return
	}

	oldParticipants := scholarship.Participants.([]*models.Participant)
	oldIDs := []bson.ObjectId{}
	var oldID bson.ObjectId
	isFinded := false
	for _, op := range oldParticipants {
		oldIDs = append(oldIDs, participant.Id)

		if op.Student.(bson.ObjectId).Hex() == participant.Student.(string) {
			isFinded = true
			oldID = op.Id
			fmt.Println("Sudah ada")
		}
	}

	if isFinded == false {
		fmt.Println("Belum ada")
		participant.Id = bson.NewObjectId()
		participant.CreatedAt = participant.Id.Time()

		p.New(&participant)
		if err = participant.Save(); err != nil {
			return
		}

		scholarship.Participants = append(oldIDs, participant.Id)

		if err = scholarship.Save(); err != nil {
			return
		}
	} else {

		oldParticipant := models.Participant{}
		err = p.FindId(oldID).Exec(&oldParticipant)

		if err != nil {
			return
		}
		// participant.Id = oldID
		// participant.CreatedAt = participant.Id.Time()
		// participant.UpdatedAt = time.Now()

		oldParticipant.Student = participant.Student
		oldParticipant.Input = participant.Input
		oldParticipant.Output = participant.Output

		oldParticipant.Save()

		fmt.Println("Updated")
	}

	return
}

// AddPaticipants is a function for ...
func (h *Handler) AddPaticipants(c echo.Context) (err error) {
	id := c.Param("scholarship_id")

	participants := []models.Participant{}
	if err = c.Bind(&participants); err != nil {
		return
	}

	if participants == nil {
		return
	}

	err = h.participantBulk(c, id, participants)

	if err != nil {
		return
	}

	return
}

func (h *Handler) participantBulk(c echo.Context, id string, participants []models.Participant) (err error) {
	if len(participants) > 0 {
		m := models.Model{}
		m.Init()
		p := m.DB.Model("Participant")
		s := m.DB.Model("Scholarship")
		scholarship := models.Scholarship{}
		err = s.FindId(bson.ObjectIdHex(id)).Populate("Participants").Exec(&scholarship)

		if err != nil {
			return
		}

		if _, ok := scholarship.Participants.([]*models.Participant); !ok {
			fmt.Println("something went wrong during type assertion. wrong type?")
			return
		}

		oldParticipants := scholarship.Participants.([]*models.Participant)
		oldStudentIDs := []bson.ObjectId{}
		oldIDs := []bson.ObjectId{}

		for _, participant := range oldParticipants {
			oldStudentIDs = append(oldStudentIDs, participant.Student.(bson.ObjectId))
			oldIDs = append(oldIDs, participant.Id)
		}

		fmt.Println(oldStudentIDs)

		keepStudentIDs := []bson.ObjectId{}
		newIDs := []bson.ObjectId{}

		for _, participant := range participants {
			if contains(oldStudentIDs, bson.ObjectIdHex(participant.Student.(string))) {
				keepStudentIDs = append(keepStudentIDs, bson.ObjectIdHex(participant.Student.(string)))
				continue
			}

			if err = c.Validate(participant); err != nil {
				continue
			}

			participant.Id = bson.NewObjectId()
			participant.CreatedAt = participant.Id.Time()

			p.New(&participant)
			if err = participant.Save(); err != nil {
				continue
			}
			newIDs = append(newIDs, participant.Id)

		}

		for i, id := range oldStudentIDs {
			if contains(keepStudentIDs, id) {
				newIDs = append(newIDs, oldIDs[i])
				continue
			}
			err = oldParticipants[i].Delete()
			if err != nil {
				continue
			}

		}

		scholarship.Participants = newIDs
		if err = scholarship.Save(); err != nil {
			return
		}

		return
	}
	return
}

// ShowParticipants is a function for ...
func (h *Handler) ShowParticipants(c echo.Context) (err error) {
	id := c.Param("scholarship_id")

	m := models.Model{}
	m.Init()
	s := m.DB.Model("Scholarship")
	scholarship := models.Scholarship{}
	err = s.FindId(bson.ObjectIdHex(id)).Exec(&scholarship)

	if err != nil {
		return
	}

	p := m.DB.Model("Participant")

	participants := []*models.Participant{}
	query := bson.M{
		"$and": []bson.M{ // you can try this in []interface
			bson.M{"deleted": false},
			bson.M{"_id": bson.M{"$in": scholarship.Participants.([]bson.ObjectId)}},
		},
	}
	fmt.Println(scholarship.Participants.([]bson.ObjectId))
	err = p.Find(query).Populate("Student").Sort([]string{"-output.total"}...).Exec(&participants)

	if err != nil {
		return
	}

	return c.JSON(http.StatusOK, participants)
}

// ShowParticipant is a function for ...
func (h *Handler) ShowParticipant(c echo.Context) (err error) {
	id := c.Param("participant_id")

	m := models.Model{}
	m.Init()
	p := m.DB.Model("Participant")

	participant := models.Participant{}
	err = p.FindId(bson.ObjectIdHex(id)).Populate("Student").Exec(&participant)

	if err != nil {
		return
	}

	return c.JSON(http.StatusOK, participant)
}

// ModifyParticipant is a function for ...
func (h *Handler) ModifyParticipant(c echo.Context) (err error) {
	id := c.Param("participant_id")

	participant := models.Participant{}
	if err = c.Bind(&participant); err != nil {
		return
	}

	m := models.Model{}
	m.Init()
	p := m.DB.Model("Participant")

	oldParticipant := models.Participant{}
	err = p.FindId(bson.ObjectIdHex(id)).Exec(&oldParticipant)

	if err != nil {
		return
	}

	oldParticipant.Student = participant.Student
	oldParticipant.Input = participant.Input
	oldParticipant.Output = participant.Output

	oldParticipant.Save()

	return c.JSON(http.StatusOK, participant)
}

// ShowScholarships is a function for ...
func (h *Handler) ShowScholarships(c echo.Context) (err error) {
	f := Filter{}
	if err = c.Bind(&f); err != nil {
		return
	}

	if err = c.Validate(f); err != nil {
		return
	}

	m := models.Model{}
	m.Init()
	s := m.DB.Model("Scholarship")
	scholarships := []*models.Scholarship{}
	err = s.Find(bson.M{"deleted": false}).Populate("Participants").Sort(f.SortFields...).Skip(f.Skip).Limit(f.Limit).Exec(&scholarships)

	if err != nil {
		return
	}
	return c.JSON(http.StatusOK, scholarships)
}

// DeleteScholarship is a function for ...
func (h *Handler) DeleteScholarship(c echo.Context) (err error) {
	id := c.Param("scholarship_id")

	m := models.Model{}
	m.Init()
	p := m.DB.Model("Scholarship")

	scholarship := models.Scholarship{}
	err = p.FindId(bson.ObjectIdHex(id)).Exec(&scholarship)
	if err != nil {
		return
	}
	err = scholarship.Delete()
	if err != nil {
		return
	}

	err = scholarship.Save()
	if err != nil {
		return
	}

	return c.JSON(http.StatusOK, scholarship)
}

// ShowScholarship is a function for ...
func (h *Handler) ShowScholarship(c echo.Context) (err error) {
	id := c.Param("scholarship_id")

	m := models.Model{}
	m.Init()
	p := m.DB.Model("Scholarship")

	scholarship := models.Scholarship{}

	err = p.FindOne(bson.M{
		"$and": []bson.M{
			bson.M{"_id": bson.ObjectIdHex(id)},
			bson.M{"deleted": false},
		},
	}).Exec(&scholarship)

	if err != nil {
		return
	}

	return c.JSON(http.StatusOK, scholarship)
}

// CalculateScholarship is a function for ...
func (h *Handler) CalculateScholarship(c echo.Context) (err error) {
	id := c.Param("scholarship_id")

	m := models.Model{}
	m.Init()
	s := m.DB.Model("Scholarship")
	scholarship := models.Scholarship{}
	err = s.FindId(bson.ObjectIdHex(id)).Populate("Participants").Exec(&scholarship)

	if err != nil {
		return
	}

	switch scholarship.Metode {
	case "AHP":
		ag := algorithm.AHPGroup{}
		res, err := ag.Hitung(scholarship.Parameters, scholarship.Participants)
		if err != nil {
			return err
		}

		m := models.Model{}
		m.Init()
		p := m.DB.Model("Participant")

		for _, participant := range res.([]models.Participant) {
			temp := models.Participant{}
			p.FindId(participant.Id).Exec(&temp)
			temp.Output = participant.Output
			temp.Save()
		}
		return c.JSON(http.StatusOK, res)
	default:
	}
	return
}

// CalculateScholarshipDemo is a function for ...
func (h *Handler) CalculateScholarshipDemo(c echo.Context) (err error) {
	id := c.Param("scholarship_id")

	m := models.Model{}
	m.Init()
	s := m.DB.Model("Scholarship")
	scholarship := models.Scholarship{}
	err = s.FindId(bson.ObjectIdHex(id)).Populate("Participants").Exec(&scholarship)

	if err != nil {
		return
	}

	switch scholarship.Metode {
	case "AHP":
		ag := algorithm.AHPGroup{}
		res, err := ag.Hitung(scholarship.Parameters, scholarship.Participants)
		if err != nil {
			return err
		}

		m := models.Model{}
		m.Init()
		p := m.DB.Model("Participant")

		for _, participant := range res.([]models.Participant) {
			temp := models.Participant{}
			p.FindId(participant.Id).Exec(&temp)
			temp.Output = participant.Output
			temp.Save()
		}
		data, _ := ag.FullResult()
		return c.JSON(http.StatusOK, map[string]interface{}{"AHP": data, "res": res})
	default:
	}
	return
}

// ClearScholarship is a function for ...
func (h *Handler) ClearScholarship(c echo.Context) (err error) {
	id := c.Param("scholarship_id")

	m := models.Model{}
	m.Init()
	p := m.DB.Model("Scholarship")

	scholarship := models.Scholarship{}
	err = p.FindId(bson.ObjectIdHex(id)).Exec(&scholarship)

	scholarship.Participants = []bson.ObjectId{}
	scholarship.Save()

	if err != nil {
		return
	}

	return c.JSON(http.StatusOK, scholarship)
}

func contains(slice []bson.ObjectId, item bson.ObjectId) bool {
	set := make(map[bson.ObjectId]struct{}, len(slice))
	for _, s := range slice {
		set[s] = struct{}{}
	}

	_, ok := set[item]
	return ok
}
