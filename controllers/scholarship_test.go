package controllers

import "testing"

func TestCreateScholarship(t *testing.T) {
	url := "http://localhost:9090/api/v1/scholarship"

	tests := []TestStruct{
		// {`{}`, BadRequestCode, "", 0},
		// {`{"type":"Beasiswa Kurang Mampu"}`, BadRequestCode, "", 0},
		// {`{"type":"Beasiswa Kurang Mampu","metode":"AHP"}`, BadRequestCode, "", 0},
		{`{"type":"Beasiswa Kurang Mampu","metode":"AHP", "participants":[],
		"data":"-"}`, SuccessRequestCode, "", 0},
	}

	DoTest(url, tests, t)
	DisplayTestCaseResults("CreateScholarship", tests, t)
}
func TestAddData(t *testing.T) {
	url := "http://localhost:9090/api/v1/scholarship/593e473a7cc0800eb2f6b5ef/data"
	tests := []TestStruct{
		// {`[]`, BadRequestCode, "", 0},
		// {`[{}]`, BadRequestCode, "", 0},
		// {`[{"name":"criteria","parent":"root","type":"criteria","criteria":["gaji","absensi","nilai","perilaku"],"questionnaire":[{"n1":"absensi","identitas":2,"n2":"gaji"},{"n1":"nilai","identitas":3,"n2":"gaji"},{"n1":"perilaku","identitas":3,"n2":"gaji"},{"n1":"nilai","identitas":2,"n2":"absensi"},{"n1":"perilaku","identitas":3,"n2":"absensi"},{"n1":"perilaku","identitas":2,"n2":"nilai"}]},{"name":"sub criteria","parent":"criteria","type":"criteria","criteria":["sangat baik","baik","cukup"],"questionnaire":[{"n1":"baik","identitas":2,"n2":"sangat baik"},{"n1":"cukup","identitas":3,"n2":"sangat baik"},{"n1":"cukup","identitas":2,"n2":"baik"}]},{"name":"gaji","parent":"root","type":"satuan criteria","criteria":["sangat baik","baik","cukup"],"questionnaire":[{"n1":"baik","identitas":3,"n2":"sangat baik"},{"n1":"cukup","identitas":3,"n2":"sangat baik"},{"n1":"cukup","identitas":3,"n2":"baik"}]},{"name":"absensi","parent":"root","type":"satuan criteria","criteria":["sangat baik","baik","cukup"],"questionnaire":[{"n1":"baik","identitas":3,"n2":"sangat baik"},{"n1":"cukup","identitas":2,"n2":"sangat baik"},{"n1":"cukup","identitas":3,"n2":"baik"}]},{"name":"nilai","parent":"root","type":"satuan criteria","criteria":["sangat baik","baik","cukup"],"questionnaire":[{"n1":"baik","identitas":2,"n2":"sangat baik"},{"n1":"cukup","identitas":2,"n2":"sangat baik"},{"n1":"cukup","identitas":3,"n2":"baik"}]},{"name":"perilaku","parent":"root","type":"satuan criteria","criteria":["sangat baik","baik","cukup"],"questionnaire":[{"n1":"baik","identitas":2,"n2":"sangat baik"},{"n1":"cukup","identitas":3,"n2":"sangat baik"},{"n1":"cukup","identitas":2,"n2":"baik"}]}]`,
		{
			`[{"name":"criteria","parent":"root","type":"criteria","criteria":[{"name":"gaji","condition":"","value":0},{"name":"absensi","condition":"","value":0},{"name":"nilai","condition":"","value":0},{"name":"perilaku","condition":"","value":0}],"questionnaire":[{"n2":"absensi","identitas":2,"n1":"gaji"},{"n2":"nilai","identitas":3,"n1":"gaji"},{"n2":"perilaku","identitas":3,"n1":"gaji"},{"n2":"nilai","identitas":2,"n1":"absensi"},{"n2":"perilaku","identitas":3,"n1":"absensi"},{"n2":"perilaku","identitas":2,"n1":"nilai"}]},{"name":"sub criteria","parent":"criteria","type":"criteria","criteria":[{"name":"sangat baik","condition":"","value":3},{"name":"baik","condition":"","value":2},{"name":"cukup","condition":"","value":1}],"questionnaire":[{"n2":"baik","identitas":2,"n1":"sangat baik"},{"n2":"cukup","identitas":3,"n1":"sangat baik"},{"n2":"cukup","identitas":2,"n1":"baik"}]},{"name":"gaji","parent":"root","type":"satuan criteria","criteria":[{"name":"sangat baik","condition":"0-1.000.000","value":3},{"name":"baik","condition":"1.000.001-2.000.000","value":2},{"name":"cukup","condition":"2.000.001-","value":1}],"questionnaire":[{"n2":"baik","identitas":3,"n1":"sangat baik"},{"n2":"cukup","identitas":3,"n1":"sangat baik"},{"n2":"cukup","identitas":3,"n1":"baik"}]},{"name":"absensi","parent":"root","type":"satuan criteria","criteria":[{"name":"sangat baik","condition":"0-2","value":3},{"name":"baik","condition":"3-5","value":2},{"name":"cukup","condition":"6-","value":1}],"questionnaire":[{"n2":"baik","identitas":3,"n1":"sangat baik"},{"n2":"cukup","identitas":2,"n1":"sangat baik"},{"n2":"cukup","identitas":3,"n1":"baik"}]},{"name":"nilai","parent":"root","type":"satuan criteria","criteria":[{"name":"sangat baik","condition":"81-90","value":3},{"name":"baik","condition":"71-80","value":2},{"name":"cukup","condition":"61-70","value":1}],"questionnaire":[{"n2":"baik","identitas":2,"n1":"sangat baik"},{"n2":"cukup","identitas":2,"n1":"sangat baik"},{"n2":"cukup","identitas":3,"n1":"baik"}]},{"name":"perilaku","parent":"root","type":"satuan criteria","criteria":[{"name":"sangat baik","condition":"A","value":3},{"name":"baik","condition":"B","value":2},{"name":"cukup","condition":"C","value":1}],"questionnaire":[{"n2":"baik","identitas":2,"n1":"sangat baik"},{"n2":"cukup","identitas":3,"n1":"sangat baik"},{"n2":"cukup","identitas":2,"n1":"baik"}]}]`,
			SuccessRequestCode,
			"",
			0,
		},
	}

	DoTest(url, tests, t)
	DisplayTestCaseResults("CreateScholarship", tests, t)
	return
}
func TestAddParticipants(t *testing.T) {
	url := "http://localhost:9090/api/v1/scholarship/593e473a7cc0800eb2f6b5ef/participants"

	tests := []TestStruct{
		// {`[]`, BadRequestCode, "", 0},
		// {`[{}]`, BadRequestCode, "", 0},
		{`[ 

{ "student": "593e46367cc0800d214ba540", "input": { "gaji": 3, "absensi": 2, "nilai": 2, "perilaku": 1 }},
{ "student": "593e46367cc0800d214ba541", "input": { "gaji": 3, "absensi": 2, "nilai": 2, "perilaku": 2 }},
{ "student": "593e46367cc0800d214ba542", "input": { "gaji": 3, "absensi": 2, "nilai": 3, "perilaku": 2 }},
{ "student": "593e46367cc0800d214ba543", "input": { "gaji": 1, "absensi": 2, "nilai": 3, "perilaku": 2 }},
{ "student": "593e46367cc0800d214ba544", "input": { "gaji": 2, "absensi": 2, "nilai": 2, "perilaku": 1 }},
{ "student": "593e46367cc0800d214ba545", "input": { "gaji": 2, "absensi": 2, "nilai": 2, "perilaku": 2 }},
{ "student": "593e46367cc0800d214ba546", "input": { "gaji": 1, "absensi": 2, "nilai": 3, "perilaku": 2 }},
{ "student": "593e46367cc0800d214ba547", "input": { "gaji": 3, "absensi": 2, "nilai": 2, "perilaku": 2 }},
{ "student": "593e46367cc0800d214ba548", "input": { "gaji": 2, "absensi": 2, "nilai": 2, "perilaku": 2 }}

]`, SuccessRequestCode, "", 0},
	}

	DoTest(url, tests, t)
	DisplayTestCaseResults("CreateScholarship", tests, t)
}

func TestAddParticipant(t *testing.T) {
	url := "http://localhost:9090/api/v1/scholarship/593e473a7cc0800eb2f6b5ef/participant"

	tests := []TestStruct{
		// {`[]`, BadRequestCode, "", 0},
		// {`[{}]`, BadRequestCode, "", 0},
		{`{ "student": "593e46367cc0800d214ba540", "input": { "gaji": 3, "absensi": 2, "nilai": 2, "perilaku": 1 }}`, SuccessRequestCode, "", 0},
	}

	DoTest(url, tests, t)
	DisplayTestCaseResults("CreateScholarship", tests, t)
}
