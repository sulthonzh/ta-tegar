package controllers

import (
	"net/http"

	"github.com/labstack/echo"
)

// PageIndex is a function for ...
func (h *Handler) PageIndex(c echo.Context) (err error) {

	data := map[string]interface{}{
		"page": "dashboard.html",
		"app":  h.GlobalData["app"],
		"head": map[string]interface{}{
			"title": "TA | Penentuan Penerimaan Beasiswa",
		},
		"leftside": h.GlobalData["leftside"],
		"topnav":   h.GlobalData["topnav"],
		"contents": "---",
		"user":     h.GlobalData["user"],
	}
	return c.Render(http.StatusOK, "main.html", data)
}
