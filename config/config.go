package config

import (
	"fmt"
	"strconv"

	"github.com/spf13/viper"
)

const (
	// Version is a ...
	Version string = "v0.0.1" // software information.
	// Author is a ...
	Author string = "TA Team" // software information.
	// Name is a ...
	Name string = "TA adalah aplikasi khusus beasiswa untuk sekolah" // software information.
	// FullName is a ...
	FullName string = Name + "_" + Version + "（by " + Author + ")" // software information.
	// Tag is a ...
	Tag string = "ta" // software information.
	// IconPNG is a ...
	IconPNG string = ""
)

const (
	// WorkRoot is a ...
	WorkRoot string = Tag + "_pkg" // software information.
	// Config is a ...
	Config string = WorkRoot + "/app.yaml" // software information.
	// CacheDir is a ...
	CacheDir string = WorkRoot + "/cache" // software information.
	// Log is a ...
	Log string = WorkRoot + "/logs/" + Tag + ".log" // software information.
	// LogAsync is a ...
	LogAsync bool = true // software information.
)

// Config is a variable for store config
var (
	RunMode       string
	Port          int
	Host          string
	ServerAddress string
)

// Config is a variable for main DB
var (
	MongoDBHost         string
	MongoDBPort         int
	MongoDBUserName     string
	MongoDBUserPassword string
	MongoDBName         string
)

// Init is a function for load
func Init() {
	viper.SetConfigName("config") // name of config file (without extension)
	viper.AddConfigPath(WorkRoot) // path to look for the config file i
	// Find and read the config file
	err := viper.ReadInConfig()

	if err != nil {
		fmt.Println("No configuration file loaded - using defaults")
	}

	RunMode = viper.GetString("global.runmode")

	Host = viper.GetString(RunMode + ".host")
	Port = viper.GetInt(RunMode + ".port")
	fmt.Println(RunMode)
	ServerAddress = Host + ":" + strconv.Itoa(Port)

	// Main
	MongoDBHost = viper.GetString("mongo.host")
	MongoDBPort = viper.GetInt("mongo.port")
	MongoDBName = viper.GetString("mongo.name")
	MongoDBUserName = viper.GetString("mongo.userName")
	MongoDBUserPassword = viper.GetString("mongo.password")
}
