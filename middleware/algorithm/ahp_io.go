package algorithm

import "gopkg.in/mgo.v2/bson"

type (
	// Questionnaire is a struct for ...
	Questionnaire struct {
		N1        string  `json:"n1" bson:"n1"`
		Identitas float64 `json:"identitas" bson:"identitas"`
		N2        string  `json:"n2" bson:"n2"`
	}
	// Parameter is a struct for ...
	Parameter struct {
		Name          string          `json:"name" bson:"name"`
		Parent        string          `json:"parent" bson:"parent"`
		Type          string          `json:"type" bson:"type"`
		Criteria      []Criteria      `json:"criteria" bson:"criteria"`
		Questionnaire []Questionnaire `json:"questionnaire" bson:"questionnaire"`
	}

	// AHP is a struct for ...
	AHP struct {
		parameter   Parameter
		n           float64
		m1          [][]float64
		sumColM1    []float64
		m2          [][]float64
		sumRowM2    []float64
		pvM2        []float64
		m3          [][]float64
		sumRowM3    []float64
		rvM3        []float64
		lmaks       float64
		ci          float64
		cr          float64
		konsistensi bool
	}

	// Criteria is a struct for ...
	Criteria struct {
		Name      string
		Condition string
		Value     int
	}
	// PenilaianSiswa is a struct for ...
	PenilaianSiswa struct {
		IDSiswa bson.ObjectId

		Gaji     float64
		Absensi  float64
		Nilai    float64
		Perilaku float64
		Total    float64
	}

// 	Public Class PenilaianSiswa
//     Implements INotifyPropertyChanged

//     Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
//     Private _nilai As Double

//     Public Property Nama As String
//     Public Property Gaji As Double
//     Public Property Absensi As Double
//     Public Property Nilai As Double
//         Get
//             Return _nilai
//         End Get
//         Set(value As Double)
//             _nilai = value

//             RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs("Nilai"))
//         End Set
//     End Property
//     Public Property Perilaku As Double
//     Public Property Total As Double

// End Class
)

// Class SiswaCollection
//     Inherits List(Of Siswa)
// End Class

// Class EnumToObjectArray
//     Inherits MarkupExtension
//     Public Property SourceEnum() As BindingBase
//         Get
//             Return m_SourceEnum
//         End Get
//         Set
//             m_SourceEnum = Value
//         End Set
//     End Property
//     Private m_SourceEnum As BindingBase

//     Public Overrides Function ProvideValue(serviceProvider As IServiceProvider) As Object
//         Dim target As IProvideValueTarget = TryCast(serviceProvider.GetService(GetType(IProvideValueTarget)), IProvideValueTarget)
//         Dim targetObject As DependencyObject
//         Dim targetProperty As DependencyProperty

//         If target IsNot Nothing AndAlso TypeOf target.TargetObject Is DependencyObject AndAlso TypeOf target.TargetProperty Is DependencyProperty Then
//             targetObject = DirectCast(target.TargetObject, DependencyObject)
//             targetProperty = DirectCast(target.TargetProperty, DependencyProperty)
//         Else
//             Return Me
//         End If

//         BindingOperations.SetBinding(targetObject, EnumToObjectArray.SourceEnumBindingSinkProperty, m_SourceEnum)

//         Dim type = targetObject.GetValue(SourceEnumBindingSinkProperty).[GetType]()

//         If type.BaseType <> GetType(System.Enum) Then
//             Return Me
//         End If

//         Return [Enum].GetValues(type).Cast(Of [Enum])().[Select](Function(e) New With {
//             Key .Value = e,
//             Key .Name = e.ToString(),
//             Key .DisplayName = Description(e)
//         })
//     End Function

//     Private Shared SourceEnumBindingSinkProperty As DependencyProperty = DependencyProperty.RegisterAttached("SourceEnumBindingSink", GetType([Enum]), GetType(EnumToObjectArray), New FrameworkPropertyMetadata(Nothing, FrameworkPropertyMetadataOptions.[Inherits]))

//     ''' <summary>
//     ''' Extension method which returns the string specified in the Description attribute, if any.  Oherwise, name is returned.
//     ''' </summary>
//     ''' <param name="value">The enum value.</param>
//     ''' <returns></returns>
//     Public Shared Function Description(value As [Enum]) As String
//         Dim attrs = value.[GetType]().GetField(value.ToString()).GetCustomAttributes(GetType(DescriptionAttribute), False)
//         If attrs.Any() Then
//             Return TryCast(attrs.First(), DescriptionAttribute).Description
//         End If

//         'Fallback
//         Return value.ToString().Replace("_", " ")
//     End Function
// End Class

// Class AHPGroup

//     Private _ahp As List(Of AHP)
//     Private _kriteria As List(Of CriteriaCollection)
//     Private _calcKriteria As List(Of AHP)

//     Private _siswa As SiswaCollection
//     Private _matriks As List(Of List(Of Double))
//     Public Event CheckconsistencyCompleted(result As List(Of AHP))
//     Public Event CalcCompleted(result As List(Of PenilaianSiswa))

//     Public ReadOnly Property GetCriteria()
//         Get
//             Return _calcKriteria
//         End Get
//     End Property

//     Public ReadOnly Property MatrikAkhir() As List(Of List(Of Double))
//         Get
//             Return _matriks
//         End Get
//     End Property

//     Public Sub Hitung()
//         Task.Run(Sub()

//                      Dim temp As PenilaianSiswa
//                      Dim hasil = _siswa.Select(Function(s, i)
//                                                    temp = New PenilaianSiswa

//                                                    With temp
//                                                        .Nama = s.Nama
//                                                        .Gaji = s.Gaji * _matriks(((s.Gaji - 3) * -1))(0)
//                                                        .Absensi = s.Absensi * _matriks(((s.Absensi - 3) * -1))(1)
//                                                        .Nilai = s.Nilai * _matriks(((s.Nilai - 3) * -1))(2)
//                                                        .Perilaku = s.Perilaku * _matriks(((s.Perilaku - 3) * -1))(3)
//                                                        .Total = .Gaji + .Absensi + .Nilai + .Perilaku

//                                                    End With
//                                                    Return temp
//                                                End Function).OrderByDescending(Function(s) s.Total).ToList

//                      ' Console.WriteLine(JsonConvert.SerializeObject(_matriks))
//                      'Console.WriteLine(JsonConvert.SerializeObject(_siswa))
//                      'Console.WriteLine(JsonConvert.SerializeObject(hasil))
//                      RaiseEvent CalcCompleted(hasil)
//                  End Sub)
//     End Sub

//     Public ReadOnly Property Data As List(Of AHP)
//         Get
//             Return _calcKriteria
//         End Get
//     End Property
// End Class

// #End Region
// End Class
