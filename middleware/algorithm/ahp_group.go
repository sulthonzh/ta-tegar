package algorithm

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/apcera/termtables"

	"gitlab.com/sulthonzh/tegar/models"
)

func (ag *AHPGroup) setLastMatrix(m [][]float64, pv []float64) {
	n := len(m)

	if n > 0 {

		ag.matriks = [][]float64{}

		for j := 0; j < len(m[0]); j++ {
			ag.matriks = append(ag.matriks, make([]float64, n))
		}

		for i := 0; i < n; i++ {
			for j := 0; j < len(m[i]); j++ {
				ag.matriks[j][i] = m[i][j] * pv[i]
			}
		}

	}
}

// Hitung adalah funsi untuk menghitung tiap siswa
func (ag *AHPGroup) Hitung(data, participant interface{}) (r interface{}, err error) {
	// Cek Koleksi Kriteria
	byteData, err := json.Marshal(data)
	if err != nil {
		return
	}

	parameters := []Parameter{}
	err = json.Unmarshal(byteData, &parameters)
	if err != nil {
		return
	}

	ag.parameters = parameters

	byteParticipant, err := json.Marshal(participant)
	if err != nil {
		return
	}
	p := []models.Participant{}
	err = json.Unmarshal(byteParticipant, &p)
	if err != nil {
		return
	}

	// Mulai hitung AHP
	ag.calcKriteria = make([]AHP, len(ag.parameters))
	for i := 0; i < len(ag.parameters); i++ {
		ahp := AHP{}
		ahp.New(ag.parameters[i])
		ahp.Hitung()
		ag.calcKriteria[i] = ahp
		ag.ahp = append(ag.ahp, ahp)
	}

	pvCriteria := []float64{}
	for _, m := range ag.calcKriteria {
		if m.parameter.Type == "criteria" && m.parameter.Parent == "root" {
			pvCriteria = m.pvM2
			break
		}
	}

	mTemp := [][]float64{}
	iTemp := 0
	for i := 0; i < len(ag.calcKriteria); i++ {
		if ag.calcKriteria[i].parameter.Type == "satuan criteria" {
			fmt.Println("mTemp", ag.calcKriteria[i].pvM2)
			mTemp = append(mTemp, ag.calcKriteria[i].pvM2)
			iTemp++
		}
	}

	ag.setLastMatrix(mTemp, pvCriteria)

	// j, _ := json.Marshal(ag.matriks)
	// fmt.Printf("\n\"Data LM\": %d - %v\n\n", 0, string(j))

	for i, s := range p {
		s.Output = map[string]float64{}

		i1 := s.Input.Gaji
		i2 := float64(i1)
		s.Output["gaji"] = i2 * ag.matriks[((i1 - 3) * -1)][0]

		i1 = s.Input.Absensi
		i2 = float64(i1)
		s.Output["absensi"] = i2 * ag.matriks[((i1 - 3) * -1)][1]

		i1 = s.Input.Nilai
		i2 = float64(i1)
		s.Output["nilai"] = i2 * ag.matriks[((i1 - 3) * -1)][2]

		i1 = s.Input.Perilaku
		i2 = float64(i1)
		s.Output["perilaku"] = i2 * ag.matriks[((i1 - 3) * -1)][3]

		s.Output["total"] = s.Output["gaji"] + s.Output["absensi"] + s.Output["nilai"] + s.Output["perilaku"]

		p[i].Output = s.Output

	}

	r = p

	return
}

// FullResult adalah fungsi untuk mengambil hasil perhitungan AHP
func (ag *AHPGroup) FullResult() (r interface{}, err error) {
	ahps := []interface{}{}

	for _, ahp := range ag.ahp {
		ahps = append(ahps, ahp.ShowAll())
	}

	data := map[string]interface{}{
		"parameters":      ag.parameters,
		"algorithmResult": ahps,
		"lastMatrix":      ag.lastMatriksText(ag.parameters),
	}

	r = data
	return
}

func (ag *AHPGroup) lastMatriksText(parameters []Parameter) (r string) {
	table := termtables.CreateTable()

	c := make([]interface{}, len(parameters[0].Criteria)+1)
	c[0] = "AHP"
	for i, v := range parameters[0].Criteria {
		c[i+1] = strings.ToUpper(v.Name)
	}

	label := make([]interface{}, len(parameters[2].Criteria))
	for i, v := range parameters[2].Criteria {
		label[i] = strings.ToUpper(v.Name)
	}

	table.AddTitle("MATRIK FINAL")
	table.AddHeaders(c...)
	fmt.Println(ag.matriks)
	for i, m := range ag.matriks {
		s := make([]interface{}, len(m)+1)
		s[0] = label[i]
		for i, v := range m {
			s[i+1] = v
		}
		table.AddRow(s...)
	}

	for i := range c {
		table.SetAlign(termtables.AlignRight, i+2)
	}
	return table.Render()
}
