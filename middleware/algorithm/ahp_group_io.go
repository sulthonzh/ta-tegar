package algorithm

type (
	// AHPGroup is a struct for ...
	AHPGroup struct {
		ahp          []AHP
		parameters   []Parameter
		calcKriteria []AHP
		matriks      [][]float64
	}
)
