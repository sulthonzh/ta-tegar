package algorithm

import (
	"fmt"
	"strings"

	"github.com/apcera/termtables"
)

var irList map[int]float64

// New ahp init
func (ahp *AHP) New(p Parameter) {
	ahp.parameter = p
	irList = map[int]float64{
		1: 0,
		2: 0,
		3: 0.58,
		4: 0.9,
		5: 1.12,
	}
}

// Hitung ahp calculate
func (ahp *AHP) Hitung() {
	ahp.cekJumlahKriteria()
	ahp.buatMatrik(1)
	ahp.buatMatrik(2)
	ahp.calcPV(2)
	ahp.buatMatrik(3)
	ahp.calcPV(3)
	ahp.lMax()
	ahp.hitungCI()
	ahp.hitungCR()
	ahp.cekKonsistensi()
}

func (ahp *AHP) cekKonsistensi() {
	ahp.konsistensi = (ahp.cr < 0.1)
}

func (ahp *AHP) hitungCR() {
	ahp.cr = ahp.ci / irList[int(ahp.n)]
}

func (ahp *AHP) hitungCI() {
	ahp.ci = (ahp.lmaks - ahp.n) / (ahp.n - 1)
}

func (ahp *AHP) cekJumlahKriteria() {
	ahp.n = ahp.parameter.Len()
}

func (ahp *AHP) columnsSum(m [][]float64) (output []float64) {
	output = make([]float64, len(m))
	for i := 0; i < len(m); i++ {
		for j := 0; j < len(m[i]); j++ {
			output[j] += m[i][j]
		}
	}
	return
}

func (ahp *AHP) rowsSum(m [][]float64) (output []float64) {
	output = make([]float64, len(m))
	for i := 0; i < len(m); i++ {
		for j := 0; j < len(m[i]); j++ {
			output[i] += m[i][j]
		}
	}
	return
}

func (ahp *AHP) sum(data []float64) (output float64) {
	for i := 0; i < len(data); i++ {
		output += data[i]
	}
	return
}

func (ahp *AHP) lMax() {
	ahp.lmaks = ahp.sum(ahp.rvM3) / ahp.n
}

func (ahp *AHP) calcPV(i int) {
	switch i {
	case 2:
		ahp.pvM2 = make([]float64, len(ahp.sumRowM2))
		for i := 0; i < len(ahp.sumRowM2); i++ {
			fmt.Println("calPV", ahp.sumRowM2[i]/ahp.n)

			ahp.pvM2[i] = ahp.sumRowM2[i] / ahp.n
		}
	case 3:
		ahp.rvM3 = make([]float64, len(ahp.sumRowM3))
		for i := 0; i < len(ahp.sumRowM3); i++ {
			ahp.rvM3[i] = ahp.sumRowM3[i] / ahp.n
		}
	default:

	}

}

func (ahp *AHP) buatMatrik(i int) {
	switch i {
	case 1:

		m := make([][]float64, len(ahp.parameter.Criteria))
		for i := 0; i < len(ahp.parameter.Criteria); i++ {
			row := make([]float64, len(ahp.parameter.Criteria))
			for j := 0; j < len(ahp.parameter.Criteria); j++ {
				col := float64(-1.0)
				for _, q := range ahp.parameter.Questionnaire {
					fmt.Println("m1-col-iden", "(", q.N1, "-", q.N2, ")", "=", 1.0, "/", q.Identitas)
					if q.N1 == ahp.parameter.Criteria[j].Name && q.N2 == ahp.parameter.Criteria[i].Name {
						col = 1.0 / q.Identitas
						break
					} else if q.N1 == ahp.parameter.Criteria[i].Name && q.N2 == ahp.parameter.Criteria[j].Name {
						col = q.Identitas
						break
					} else {
						col = 1.0
					}
				}

				row[j] = col
			}
			m[i] = row
		}
		ahp.m1 = m

		ahp.sumColM1 = ahp.columnsSum(m)

	case 2:
		m := make([][]float64, len(ahp.m1))
		for i := 0; i < len(ahp.m1); i++ {

			row := make([]float64, len(ahp.m1[i]))
			for j := 0; j < len(ahp.m1[i]); j++ {
				row[j] = ahp.m1[i][j] / ahp.sumColM1[j]
				println(row[j])
			}

			m[i] = row
		}
		ahp.m2 = m
		ahp.sumRowM2 = ahp.rowsSum(m)

	case 3:
		m := make([][]float64, len(ahp.m1))
		for i := 0; i < len(ahp.m1); i++ {
			row := make([]float64, len(ahp.m1[i]))
			for j := 0; j < len(ahp.m1[i]); j++ {
				row[j] = ahp.m1[i][j] * ahp.pvM2[i]
			}
			m[i] = row
		}
		ahp.m3 = m
		ahp.sumRowM3 = ahp.rowsSum(m)

	default:
	}

}

// Len is function to get criteria count
func (c *Parameter) Len() float64 {
	return float64(len(c.Criteria))
}

// ShowAll is function to get criteria count
func (ahp *AHP) ShowAll() (r interface{}) {
	data := []interface{}{
		ahp.m1Text(),
		ahp.m2Text(),
		ahp.m3Text(),
		ahp.pvrvText(),
		ahp.conclusionText(),
	}
	r = data
	return
}

func (ahp *AHP) m1Text() (r string) {
	table := termtables.CreateTable()

	c := make([]interface{}, len(ahp.parameter.Criteria)+1)

	c[0] = strings.ToUpper(ahp.parameter.Name)
	for i, v := range ahp.parameter.Criteria {

		c[i+1] = strings.ToUpper(v.Name)
	}

	table.AddTitle("MATRIK 1")
	table.AddHeaders(c...)
	for i, m := range ahp.m1 {
		s := make([]interface{}, len(m)+1)
		s[0] = c[i+1]
		for i, v := range m {
			s[i+1] = v
		}

		table.AddRow(s...)
	}
	table.AddSeparator()

	s := make([]interface{}, len(ahp.sumColM1)+1)
	s[0] = "JUMLAH"
	for i, v := range ahp.sumColM1 {
		s[i+1] = v
	}
	table.AddRow(s...)

	for i := range ahp.sumColM1 {
		table.SetAlign(termtables.AlignRight, i+2)
	}

	return table.Render() // strings.Replace(table.Render(), "\n", "&#13;&#10;", -1)
}

func (ahp *AHP) m2Text() (r string) {
	table := termtables.CreateTable()

	c := make([]interface{}, len(ahp.parameter.Criteria)+3)

	c[0] = strings.ToUpper(ahp.parameter.Name)
	for i, v := range ahp.parameter.Criteria {
		c[i+1] = strings.ToUpper(v.Name)
	}
	c[len(ahp.parameter.Criteria)+1] = "JUMLAH"
	c[len(ahp.parameter.Criteria)+2] = "PV"

	table.AddTitle("MATRIK 2")
	table.AddHeaders(c...)
	for i, m := range ahp.m2 {
		s := make([]interface{}, len(m)+3)
		s[0] = c[i+1]
		for i, v := range m {
			s[i+1] = v
		}

		s[len(m)+1] = ahp.sumRowM2[i]
		s[len(m)+2] = ahp.pvM2[i]
		table.AddRow(s...)
	}

	for i := range c {
		table.SetAlign(termtables.AlignRight, i+2)
	}

	return table.Render() //strings.Replace(table.Render(), "\n", "&#13;&#10;", -1)
}

func (ahp *AHP) m3Text() (r string) {
	table := termtables.CreateTable()

	c := make([]interface{}, len(ahp.parameter.Criteria)+3)

	c[0] = strings.ToUpper(ahp.parameter.Name)
	for i, v := range ahp.parameter.Criteria {
		c[i+1] = strings.ToUpper(v.Name)
	}
	c[len(ahp.parameter.Criteria)+1] = "JUMLAH"
	c[len(ahp.parameter.Criteria)+2] = "RV"

	table.AddTitle("MATRIK 3")
	table.AddHeaders(c...)
	for i, m := range ahp.m3 {
		s := make([]interface{}, len(m)+3)
		s[0] = c[i+1]
		for i, v := range m {
			s[i+1] = v
		}

		s[len(m)+1] = ahp.sumRowM3[i]
		s[len(m)+2] = ahp.rvM3[i]
		table.AddRow(s...)
	}

	for i := range c {
		table.SetAlign(termtables.AlignRight, i+2)
	}

	return table.Render() //strings.Replace(table.Render(), "\n", "&#13;&#10;", -1)
}

func (ahp *AHP) pvrvText() (r string) {
	table := termtables.CreateTable()

	c := make([]interface{}, 3)

	c[0] = strings.ToUpper(ahp.parameter.Name)
	c[1] = "PRIORITAS"
	c[2] = "RATIO"

	table.AddTitle("PRIORITAS DAN RATIO")
	table.AddHeaders(c...)
	for i, m := range ahp.parameter.Criteria {
		s := make([]interface{}, 3)
		s[0] = strings.ToUpper(m.Name)
		s[1] = ahp.pvM2[i]
		s[2] = ahp.rvM3[i]
		table.AddRow(s...)
	}
	table.AddSeparator()
	s := make([]interface{}, 3)
	s[0] = "JUMLAH"
	s[1] = ""
	s[2] = ahp.sum(ahp.rvM3)
	table.AddRow(s...)

	for i := range c {
		table.SetAlign(termtables.AlignRight, i+2)
	}

	return table.Render() //strings.Replace(table.Render(), "\n", "&#13;&#10;", -1)
}

func (ahp *AHP) conclusionText() (r string) {
	table := termtables.CreateTable()

	c := make([]interface{}, 2)

	c[0] = "LABEL"
	c[1] = "VALUE"

	table.AddTitle("KESIMPULAN")
	table.AddHeaders(c...)

	label := []string{"JUMLAH KRITERIA", "LAMBDA MAX (λ MAX)", "CI", "CR", "IR", "KONSISTENSI"}
	value := []interface{}{ahp.n, ahp.lmaks, ahp.ci, ahp.cr, irList[int(ahp.n)], ahp.konsistensi}

	for i, m := range label {
		s := make([]interface{}, 2)
		s[0] = m
		s[1] = value[i]
		table.AddRow(s...)
	}

	for i := range c {
		table.SetAlign(termtables.AlignRight, i+2)
	}

	return table.Render() //strings.Replace(table.Render(), "\n", "&#13;&#10;", -1)
}
