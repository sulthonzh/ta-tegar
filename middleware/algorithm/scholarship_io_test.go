package algorithm

import (
	"encoding/json"
	"fmt"
	"testing"

	"gitlab.com/sulthonzh/tegar/models"
)

func TestAHP(t *testing.T) {
	participants := []models.Participant{
		{
			Student: models.Student{
				Name: "Adam Ramadhan",
			},
			Input: models.Input{
				Gaji:     3,
				Absensi:  2,
				Nilai:    2,
				Perilaku: 1,
			},
		},
		{
			Student: models.Student{
				Name: "Ahmad David",
			},
			Input: models.Input{
				Gaji:     3,
				Absensi:  2,
				Nilai:    2,
				Perilaku: 2,
			},
		},
		{
			Student: models.Student{
				Name: "Arif Setia",
			},
			Input: models.Input{
				Gaji:     3,
				Absensi:  2,
				Nilai:    3,
				Perilaku: 2,
			},
		},
		{
			Student: models.Student{
				Name: "Athiyyah",
			},
			Input: models.Input{
				Gaji:     1,
				Absensi:  2,
				Nilai:    3,
				Perilaku: 2,
			}},
		{
			Student: models.Student{
				Name: "Dandi ",
			},
			Input: models.Input{
				Gaji:     2,
				Absensi:  2,
				Nilai:    2,
				Perilaku: 1,
			},
		},
		{
			Student: models.Student{
				Name: "David",
			},
			Input: models.Input{
				Gaji:     2,
				Absensi:  2,
				Nilai:    2,
				Perilaku: 2,
			},
		},
		{
			Student: models.Student{
				Name: "Deni",
			},
			Input: models.Input{
				Gaji:     1,
				Absensi:  2,
				Nilai:    3,
				Perilaku: 2,
			},
		},
		{
			Student: models.Student{
				Name: "Ariek",
			},
			Input: models.Input{
				Gaji:     3,
				Absensi:  2,
				Nilai:    2,
				Perilaku: 2,
			},
		},
		{
			Student: models.Student{
				Name: "Fahrizal",
			},
			Input: models.Input{
				Gaji:     2,
				Absensi:  2,
				Nilai:    2,
				Perilaku: 2,
			},
		},
	}

	parameters := []Parameter{
	// 0: {
	// 	Name: "criteria",
	// 	Criteria: []string{
	// 		0: "gaji",
	// 		1: "absensi",
	// 		2: "nilai",
	// 		3: "perilaku",
	// 	},
	// 	Parent: "root",
	// 	Questionnaire: []Questionnaire{
	// 		0: Questionnaire{
	// 			N1:        "absensi",
	// 			Identitas: 2,
	// 			N2:        "gaji",
	// 		},
	// 		1: Questionnaire{
	// 			N1:        "nilai",
	// 			Identitas: 3,
	// 			N2:        "gaji",
	// 		},
	// 		2: Questionnaire{
	// 			N1:        "perilaku",
	// 			Identitas: 3,
	// 			N2:        "gaji",
	// 		},
	// 		3: Questionnaire{
	// 			N1:        "nilai",
	// 			Identitas: 2,
	// 			N2:        "absensi",
	// 		},
	// 		4: Questionnaire{
	// 			N1:        "perilaku",
	// 			Identitas: 3,
	// 			N2:        "absensi",
	// 		},
	// 		5: Questionnaire{
	// 			N1:        "perilaku",
	// 			Identitas: 2,
	// 			N2:        "nilai",
	// 		},
	// 	},
	// 	Type: "criteria",
	// },
	// 1: {
	// 	Name: "sub criteria",
	// 	Criteria: []string{
	// 		0: "sangat baik",
	// 		1: "baik",
	// 		2: "cukup",
	// 	},
	// 	Parent: "criteria",
	// 	Questionnaire: []Questionnaire{
	// 		0: Questionnaire{
	// 			N1:        "baik",
	// 			Identitas: 2,
	// 			N2:        "sangat baik",
	// 		},
	// 		1: Questionnaire{
	// 			N1:        "cukup",
	// 			Identitas: 3,
	// 			N2:        "sangat baik",
	// 		},
	// 		2: Questionnaire{
	// 			N1:        "cukup",
	// 			Identitas: 2,
	// 			N2:        "baik",
	// 		},
	// 	},
	// 	Type: "criteria",
	// },
	// 2: {
	// 	Name: "gaji",
	// 	Criteria: []string{
	// 		0: "sangat baik",
	// 		1: "baik",
	// 		2: "cukup",
	// 	},
	// 	Parent: "root",
	// 	Questionnaire: []Questionnaire{
	// 		0: Questionnaire{
	// 			N1:        "baik",
	// 			Identitas: 3,
	// 			N2:        "sangat baik",
	// 		},
	// 		1: Questionnaire{
	// 			N1:        "cukup",
	// 			Identitas: 3,
	// 			N2:        "sangat baik",
	// 		},
	// 		2: Questionnaire{
	// 			N1:        "cukup",
	// 			Identitas: 3,
	// 			N2:        "baik",
	// 		},
	// 	},
	// 	Type: "satuan criteria",
	// },
	// 3: {
	// 	Name: "absensi",
	// 	Criteria: []string{
	// 		0: "sangat baik",
	// 		1: "baik",
	// 		2: "cukup",
	// 	},
	// 	Parent: "root",
	// 	Questionnaire: []Questionnaire{
	// 		0: Questionnaire{
	// 			N1:        "baik",
	// 			Identitas: 3,
	// 			N2:        "sangat baik",
	// 		},
	// 		1: Questionnaire{
	// 			N1:        "cukup",
	// 			Identitas: 2,
	// 			N2:        "sangat baik",
	// 		},
	// 		2: Questionnaire{
	// 			N1:        "cukup",
	// 			Identitas: 3,
	// 			N2:        "baik",
	// 		},
	// 	},
	// 	Type: "satuan criteria",
	// },
	// 4: {
	// 	Name: "nilai",
	// 	Criteria: []string{
	// 		0: "sangat baik",
	// 		1: "baik",
	// 		2: "cukup",
	// 	},
	// 	Parent: "root",
	// 	Questionnaire: []Questionnaire{
	// 		0: Questionnaire{
	// 			N1:        "baik",
	// 			Identitas: 2,
	// 			N2:        "sangat baik",
	// 		},
	// 		1: Questionnaire{
	// 			N1:        "cukup",
	// 			Identitas: 2,
	// 			N2:        "sangat baik",
	// 		},
	// 		2: Questionnaire{
	// 			N1:        "cukup",
	// 			Identitas: 3,
	// 			N2:        "baik",
	// 		},
	// 	},
	// 	Type: "satuan criteria",
	// },
	// 5: {
	// 	Name: "perilaku",
	// 	Criteria: []string{
	// 		0: "sangat baik",
	// 		1: "baik",
	// 		2: "cukup",
	// 	},
	// 	Parent: "root",
	// 	Questionnaire: []Questionnaire{
	// 		0: Questionnaire{
	// 			N1:        "baik",
	// 			Identitas: 2,
	// 			N2:        "sangat baik",
	// 		},
	// 		1: Questionnaire{
	// 			N1:        "cukup",
	// 			Identitas: 3,
	// 			N2:        "sangat baik",
	// 		},
	// 		2: Questionnaire{
	// 			N1:        "cukup",
	// 			Identitas: 2,
	// 			N2:        "baik",
	// 		},
	// 	},
	// 	Type: "satuan criteria",
	//  },
	}

	// Hasil filter beasiswa
	bea := models.Scholarship{}
	bea.Type = "Beasiswa Kurang Mampu"
	bea.Participants = participants
	bea.Metode = "AHP"
	bea.Parameters = parameters

	c, _ := json.Marshal(parameters)

	fmt.Println("=================")
	fmt.Printf("\"Data\": %v", string(c))
	fmt.Println("=================")

	// Hitung beasiswa berdasarkan data yang ada
	switch bea.Metode {
	case "AHP":
		ag := AHPGroup{}
		p, err := ag.Hitung(bea.Parameters, bea.Participants)
		if err != nil {
			println(err)
			return
		}

		j, _ := json.Marshal(p)
		fmt.Printf("\"Data\": %v", string(j))
	default:
	}

}
