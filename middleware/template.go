package middleware

import (
	"bytes"
	"errors"
	"fmt"
	"html/template"
	"io"
	"strconv"

	"gopkg.in/mgo.v2/bson"

	"strings"

	"time"

	"github.com/labstack/echo"
	"github.com/leekchan/accounting"
)

// Template is a struct for ...
type Template struct {
	Templates *template.Template
}

// Render is a function for ...
func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
	t.SetFuncs()
	return t.Templates.ExecuteTemplate(w, name, data)
}

// Init is a function for template init
func (t *Template) Init() {
	t.Templates = template.New("ta")
	t.SetFuncs()
	template.Must(t.Templates.ParseGlob("resources/views/**/*"))
}

// SetFuncs is a function for set template funcs
func (t *Template) SetFuncs() {
	t.Templates.Funcs(template.FuncMap{
		"oitos": func(oi bson.ObjectId) (s string) {
			s = oi.Hex()
			return
		},
		"iton": func(i int) (n int) {
			n = i + 1
			return
		},
		"options": func(v1, v2 string) (s bool) {
			if v1 == v2 {
				s = true
			}
			return
		},
		"voi": func(l map[string]map[int]string, key string) (ret map[int]string, err error) {
			ret = l[key]
			return
		},
		"inc": func(i int, j int) (ret int) {
			i += j
			return i
		},
		"ct": func(name string, data interface{}) (ret template.HTML, err error) {
			buf := bytes.NewBuffer([]byte{})
			err = t.Templates.ExecuteTemplate(buf, name, data)
			ret = template.HTML(buf.String())
			return
		},
		"dict": func(values ...interface{}) (map[string]interface{}, error) {
			if len(values)%2 != 0 {
				return nil, errors.New("invalid dict call")
			}
			dict := make(map[string]interface{}, len(values)/2)
			for i := 0; i < len(values); i += 2 {
				key, ok := values[i].(string)
				if !ok {
					return nil, errors.New("dict keys must be strings")
				}
				dict[key] = values[i+1]
			}
			return dict, nil
		},
		"currency": func(ac accounting.Accounting, amount interface{}) string {
			return ac.FormatMoney(amount)
		},
		"number": func(ac accounting.Accounting, amount interface{}) string {
			return ac.FormatMoney(amount)
		},
		"sort": func(s string, i int) string {
			runes := []rune(s)
			if len(runes) > i {
				return string(runes[:i]) + "..."
			}
			return s
		},
		"fuzzyChanged": func(i float64) (result string) {
			if i > 0 {
				result = " fuzzy-item"
			}
			return
		},
		"parseFloat": func(i interface{}) (result float64) {
			result, _ = strconv.ParseFloat(i.(string), 64)
			return
		},
		"minYear": func() (result int) {

			t := time.Now().Add(-(time.Hour * 24 * 30 * 12 * 10))

			return t.Year()
		},
		"join": func(sep string, s ...interface{}) string {
			str := []string{}
			for _, temp := range s {
				if temp != nil {
					if temp != "" {
						str = append(str, temp.(string))
					}
				}
			}
			return strings.Join(str, sep)
		},
		"title": strings.Title,
		"specvalid": func(s interface{}) (ok bool) {
			switch s.(type) {
			case int:
				if s.(int) > 0 {
					ok = true
				}
			case float64:
				if s.(float64) > 0 {
					ok = true
				}
			case string:
				if s.(string) != "" {
					ok = true
				}
			default:
				return false
			}
			return
		},
		"nilai": func(i int) (o string) {
			switch i {
			case 1:
				o = "Cukup"
			case 2:
				o = "Baik"
			case 3:
				o = "Sangat Baik"
			}
			return
		},
		"questionnaire": func(i float64) (o string) {
			data := map[int]string{
				1: "Sama pentingnya",
				2: "Antara sama pentingnya dan sedikit lebih penting",
				3: "Sedikit lebih penting",
				4: "Antara sedikit lebih penting dan lebih penting",
				5: "Lebih Penting",
				6: "Antara lebih penting dan sangat penting",
				7: "Sangat Penting",
				8: "Antara sangat penting dan mutlak lebih penting",
				9: "Mutlak lebih penting",
			}
			o = data[int(i)]

			return
		},
		"toString": func(value int) string {
			return strconv.Itoa(value)
		},
		"toString2": func(value interface{}) string {
			return fmt.Sprintf("%v", value)
		},
		"fd": func(t time.Time, f string) string {
			return t.Format(f)
		},
	})
}
