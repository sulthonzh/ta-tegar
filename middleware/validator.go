package middleware

import validator "gopkg.in/go-playground/validator.v9"

type (
	// CustomValidator definition.
	CustomValidator struct {
		Validator *validator.Validate
	}
)

// Validate is a func for ...
func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.Validator.Struct(i)
}
