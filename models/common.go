package models

import (
	"fmt"

	"github.com/zebresel-com/mongodm"
	"gitlab.com/sulthonzh/tegar/config"
)

// Model common
type Model struct {
	DB *mongodm.Connection
}

func (m *Model) Init() {
	localMap := map[string]map[string]string{
		"en-US": map[string]string{
			"validation.field_required":           "Field '%s' is required.",
			"validation.field_invalid":            "Field '%s' has an invalid value.",
			"validation.field_invalid_id":         "Field '%s' contains an invalid object id value.",
			"validation.field_minlen":             "Field '%s' must be at least %v characters long.",
			"validation.field_maxlen":             "Field '%s' can be maximum %v characters long.",
			"validation.entry_exists":             "%s already exists for value '%v'.",
			"validation.field_not_exclusive":      "Only one of both fields can be set: '%s'' or '%s'.",
			"validation.field_required_exclusive": "Field '%s' or '%s' required.",
		},
	}

	dbConfig := &mongodm.Config{
		DatabaseHosts:    []string{config.MongoDBHost},
		DatabaseName:     config.MongoDBName,
		DatabaseUser:     config.MongoDBUserName,
		DatabasePassword: config.MongoDBUserPassword,
		Locals:           localMap["en-US"],
	}

	connection, err := mongodm.Connect(dbConfig)

	if err != nil {
		fmt.Println("Database connection error: ", err)
		return
	}

	// Register struct
	connection.Register(&Student{}, "students")
	connection.Register(&Scholarship{}, "scholarships")
	connection.Register(&Participant{}, "participants")

	m.DB = connection
}
