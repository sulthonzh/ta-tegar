package models

import "github.com/zebresel-com/mongodm"
import "time"

type (
	// Student is a struct for ...
	Student struct {
		mongodm.DocumentBase `json:",inline" bson:",inline"`

		Name       string      `json:"name" bson:"name"`
		Class      int         `json:"class" bson:"class"`
		ClassGroup string      `json:"classGroup" bson:"classGroup"`
		Gender     int         `json:"gender" bson:"gender"`
		Birthcity  string      `json:"birthcity" bson:"birthcity"`
		Birthday   time.Time   `json:"birthday" bson:"birthday"`
		ParentInfo interface{} `json:"parentInfo" bson:"parentInfo"`
		Input      Input       `json:"input" bson:"input"`
	}

	// Input Params
	Input struct {
		Gaji     int `json:"gaji" bson:"gaji"`
		Absensi  int `json:"absensi" bson:"absensi"`
		Nilai    int `json:"nilai" bson:"nilai"`
		Perilaku int `json:"perilaku" bson:"perilaku"`
	}
)
