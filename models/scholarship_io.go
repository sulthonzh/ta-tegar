package models

import "github.com/zebresel-com/mongodm"

type (
	// Scholarship is a struct for ...
	Scholarship struct {
		mongodm.DocumentBase `json:",inline" bson:",inline"`

		Type         string      `json:"type" bson:"type"`
		Participants interface{} `json:"participants" bson:"participants" model:"Participant" relation:"1n" autosave:"true"`
		Metode       string      `json:"metode" bson:"metode"`
		Parameters   interface{} `json:"parameters" bson:"parameters"`
	}

	// Participant is a struct for ...
	Participant struct {
		mongodm.DocumentBase `json:",inline" bson:",inline"`

		Student interface{}        `json:"student" bson:"student" model:"Student" relation:"11" autosave:"true"`
		Input   Input              `json:"input" bson:"input"`
		Output  map[string]float64 `json:"output" bson:"output"`
	}
)
