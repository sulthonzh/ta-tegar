package routes

import (
	"github.com/labstack/echo"
	"gitlab.com/sulthonzh/tegar/controllers"
)

// Init is a func for ...
func Init(e *echo.Echo) {
	e.Static("/", "public")
	e.File("/favicon.ico", "public/assets/images/favicon.ico")

	// Define handler
	handler := controllers.Handler{}
	handler.New()
	// route front
	e.GET("/", handler.PageIndex)
	e.GET("/student", handler.PageStudents)
	e.GET("/student/:student_id", handler.PageStudent)
	e.GET("/scholarship", handler.PageScholarships)
	e.GET("/scholarship/:scholarship_id", handler.PageScholarship)
	e.GET("/scholarship/:scholarship_id/criteria/:index", handler.PageCriteria)
	e.GET("/scholarship/:scholarship_id/participant/:participant_id", handler.PageParticipant)
	e.GET("/demo", handler.PageDemo)

	// route API
	e.POST("/api/v1/student", handler.CreateStudent)
	e.PATCH("/api/v1/student/info", handler.UpdateStudentInfo)
	e.PATCH("/api/v1/student/:student_id", handler.UpdateStudentInput)
	e.GET("/api/v1/student", handler.ShowStudents)
	e.GET("/api/v1/student/:student_id", handler.ShowStudent)
	e.DELETE("/api/v1/student/:student_id", handler.DeleteStudent)

	e.POST("/api/v1/scholarship", handler.CreateScholarship)
	e.PATCH("/api/v1/scholarship", handler.PatchScholarship)
	e.POST("/api/v1/scholarship/:scholarship_id/participants", handler.AddPaticipants)
	e.POST("/api/v1/scholarship/:scholarship_id/participant", handler.AddPaticipant)
	e.GET("/api/v1/scholarship/:scholarship_id/participant", handler.ShowParticipants)

	e.POST("/api/v1/scholarship/:scholarship_id/data", handler.AddData)
	e.GET("/api/v1/scholarship/participant", handler.AddBulk)

	e.PATCH("/api/v1/participant/:participant_id", handler.ModifyParticipant)
	e.GET("/api/v1/participant/:participant_id", handler.ShowParticipant)

	e.GET("/api/v1/scholarship", handler.ShowScholarships)
	e.GET("/api/v1/scholarship/:scholarship_id", handler.ShowScholarship)
	e.GET("/api/v1/scholarship/:scholarship_id/calculate", handler.CalculateScholarship)
	e.GET("/api/v1/scholarship/:scholarship_id/calculate-demo", handler.CalculateScholarshipDemo)
	e.GET("/api/v1/scholarship/:scholarship_id/clear", handler.ClearScholarship)
	e.DELETE("/api/v1/scholarship/:scholarship_id", handler.DeleteScholarship)
}
